apiVersion: v1
kind: Template
labels:
  app: ldap
  template: ldap-statefulset-persistent
metadata:
  annotations:
    description: OpenLDAP Cluster
    iconClass: icon-openshift
    openshift.io/display-name: OpenLDAP Cluster
    tags: openldap,ldap
  name: ldap-statefulset-persistent
objects:
- apiVersion: apps/v1
  kind: StatefulSet
  metadata:
    name: openldap-${FRONTNAME}
  spec:
    replicas: 3
    selector:
      matchLabels:
        name: openldap-${FRONTNAME}
    serviceName: openldap-${FRONTNAME}
    template:
      metadata:
        labels:
          name: openldap-${FRONTNAME}
      spec:
        affinity:
          podAntiAffinity:
            preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                  - key: name
                    operator: In
                    values: [ "openldap-${FRONTNAME}" ]
                topologyKey: kubernetes.io/hostname
              weight: 42
        containers:
        - env:
          - name: DEBUG
            value: yay
          - name: DO_ADMIN
            value: "true"
          - name: DO_AIRSONIC
            value: "true"
          - name: DO_ALERTMANAGER
            value: "true"
          - name: DO_ARA
            value: "true"
          - name: DO_ARTIFACTORY
            value: "true"
          - name: DO_AWX
            value: "true"
          - name: DO_BLUEMIND
            value: "false"
          - name: DO_CODIMD
            value: "true"
          - name: DO_CYRUS
            value: "true"
          - name: DO_DIASPORA
            value: "true"
          - name: DO_DOKUWIKI
            value: "true"
          - name: DO_DRAW
            value: "true"
          - name: DO_ERRBIT
            value: "true"
          - name: DO_ESCOMRADE
            value: "true"
          - name: DO_ETHERPAD
            value: "true"
          - name: DO_EXPORTERS
            value: "true"
          - name: DO_FUSION
            value: "true"
          - name: DO_GERRIT
            value: "true"
          - name: DO_GITEA
            value: "true"
          - name: DO_GOGS
            value: "true"
          - name: DO_GRAFANA
            value: "true"
          - name: DO_GREENLIGHT
            value: "true"
          - name: DO_JENKINS
            value: "false"
          - name: DO_KIBANA
            value: "false"
          - name: DO_KIWIIRC
            value: "true"
          - name: DO_LEMON
            value: "true"
          - name: DO_MASTODON
            value: "true"
          - name: DO_MATOMO
            value: "true"
          - name: DO_MATTERMOST
            value: "true"
          - name: DO_MEDUSA
            value: "true"
          - name: DO_MINIO
            value: "true"
          - name: DO_NEXTCLOUD
            value: "true"
          - name: DO_NEXUS
            value: "true"
          - name: DO_PEERTUBE
            value: "true"
          - name: DO_PHPLDAPADMIN
            value: "true"
          - name: DO_PROMETHEUS
            value: "true"
          - name: DO_REGISTRYCONSOLE
            value: "true"
          - name: DO_RELEASEBELL
            value: "true"
          - name: DO_ROCKET
            value: "true"
          - name: DO_SABNZBD
            value: "true"
          - name: DO_SERVICEDESK
            value: "true"
          - name: DO_SONARQUBE
            value: "true"
          - name: DO_SOPLANNING
            value: "true"
          - name: DO_SSP
            value: "true"
          - name: DO_SYMPA
            value: "true"
          - name: DO_SYSPASS
            value: "true"
          - name: DO_THANOS
            value: "true"
          - name: DO_TINYTINYRSS
            value: "true"
          - name: DO_TRANSMISSION
            value: "true"
          - name: DO_WEKAN
            value: "true"
          - name: DO_WHITEPAGES
            value: "true"
          - name: DO_WORDPRESS
            value: "true"
          - name: DO_WSWEET
            value: "true"
          - name: LLNG_APPNAME
            value: KubeLemon
          - name: LLNG_BACKGROUND
            value: wall_dark.jpg
          - name: LLNG_LANG
            value: en
          - name: LLNG_SKIN
            value: kubelemon
          - name: OPENLDAP_ALERTMANAGER_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: alertmanager-oauth2-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ARA_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: ara-oauth2-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ARA_PASSWORD
            valueFrom:
              secretKeyRef:
                key: ara-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ARTIFACTORY_PASSWORD
            valueFrom:
              secretKeyRef:
                key: artifactory-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_AUTHPROXY_PASSWORD
            valueFrom:
              secretKeyRef:
                key: authproxy-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_AWX_PASSWORD
            valueFrom:
              secretKeyRef:
                key: awx-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_BIND_LDAP_PORT
            value: "1389"
          - name: OPENLDAP_BIND_LDAPS_PORT
            value: "1636"
          - name: OPENLDAP_BLUEMIND_PASSWORD
            valueFrom:
              secretKeyRef:
                key: bluemind-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_CODIMD_PASSWORD
            valueFrom:
              secretKeyRef:
                key: codimd-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_CYRUS_PASSWORD
            valueFrom:
              secretKeyRef:
                key: cyrus-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_DB_BACKEND
            value: mdb
          - name: OPENLDAP_DEBUG_LEVEL
            value: "256"
          - name: OPENLDAP_DOKUWIKI_PASSWORD
            valueFrom:
              secretKeyRef:
                key: dokuwiki-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_DRAW_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: draw-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ERRBIT_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: errbit-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ERRBIT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: errbit-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ESCOMRADE_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: escomrade-oauth2-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_FUSION_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: fusion-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_FUSION_PASSWORD
            valueFrom:
              secretKeyRef:
                key: fusion-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_GERRIT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: gerrit-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_GLOBAL_ADMIN_PASSWORD
            valueFrom:
              secretKeyRef:
                key: global-admin-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_GRAFANA_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: grafana-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_GRAFANA_PASSWORD
            valueFrom:
              secretKeyRef:
                key: grafana-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_GREENLIGHT_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: greenlight-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_GREENLIGHT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: greenlight-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_HOST_ENDPOINT
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_INIT_DEBUG_LEVEL
            value: "256"
          - name: OPENLDAP_JENKINS_PASSWORD
            valueFrom:
              secretKeyRef:
                key: jenkins-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_KIBANA_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: kibana-oauth2-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_KIWIIRC_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: kiwiirc-oauth2-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_LEMONLDAP_HTTPS
            value: yep
          - name: OPENLDAP_LEMONLDAP_PASSWORD
            valueFrom:
              secretKeyRef:
                key: lemonldap-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD
            valueFrom:
              secretKeyRef:
                key: lemonldap-sessions-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_LEMON_HTTP_PORT
            value: "8080"
          - name: OPENLDAP_MASTODON_PASSWORD
            valueFrom:
              secretKeyRef:
                key: mastodon-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_MATOMO_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: matomo-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_MATOMO_PASSWORD
            valueFrom:
              secretKeyRef:
                key: matomo-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_MATTERMOST_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: mattermost-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_MATTERMOST_PASSWORD
            valueFrom:
              secretKeyRef:
                key: mattermost-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_MEDUSA_PASSWORD
            valueFrom:
              secretKeyRef:
                key: medusa-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_MONITOR_PASSWORD
            valueFrom:
              secretKeyRef:
                key: monitor-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_NEXTCLOUD_PASSWORD
            valueFrom:
              secretKeyRef:
                key: nextcloud-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_NEXUS_PASSWORD
            valueFrom:
              secretKeyRef:
                key: nexus-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ORG_SHORT
            value: ${ORG_NAME}
          - name: OPENLDAP_PEERTUBE_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: peertube-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_PEERTUBE_PASSWORD
            valueFrom:
              secretKeyRef:
                key: peertube-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_PHPLDAPADMIN_PASSWORD
            valueFrom:
              secretKeyRef:
                key: phpldapadmin-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_PROMETHEUS_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: prometheus-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_REGISTRYCONSOLE_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: registryconsole-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_REGISTRYCONSOLE_PASSWORD
            valueFrom:
              secretKeyRef:
                key: registryconsole-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_RELEASEBELL_PASSWORD
            valueFrom:
              secretKeyRef:
                key: releasebell-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ROCKET_PASSWORD
            valueFrom:
              secretKeyRef:
                key: rocket-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ROOT_DN_PREFIX
            value: cn=Directory Manager
          - name: OPENLDAP_ROOT_DN_SUFFIX
            valueFrom:
              secretKeyRef:
                key: directory-root
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_ROOT_DOMAIN
            value: ${ROOT_DOMAIN}
          - name: OPENLDAP_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: root-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SABNZBD_PASSWORD
            valueFrom:
              secretKeyRef:
                key: sabnzbd-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SERVICEDESK_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: servicedesk-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SERVICEDESK_PASSWORD
            valueFrom:
              secretKeyRef:
                key: servicedesk-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SMTP_SERVER
            value: ${SMTP_RELAY}
          - name: OPENLDAP_SONARQUBE_PASSWORD
            valueFrom:
              secretKeyRef:
                key: sonarqube-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SOPLANNING_PASSWORD
            valueFrom:
              secretKeyRef:
                key: soplanning-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SSO_CLIENT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: ssoapp-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SSP_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: ssp-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SSP_PASSWORD
            valueFrom:
              secretKeyRef:
                key: ssp-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_STATEFULSET_NAME
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_SYMPA_PASSWORD
            valueFrom:
              secretKeyRef:
                key: sympa-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SYNCREPL_PASSWORD
            valueFrom:
              secretKeyRef:
                key: syncrepl-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_SYSPASS_PASSWORD
            valueFrom:
              secretKeyRef:
                key: syspass-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_THANOS_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: thanos-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_THANOSBUCKET_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: thanosbucket-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_THANOSRECEIVE_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: thanosreceive-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_TINYTINYRSS_PASSWORD
            valueFrom:
              secretKeyRef:
                key: tinytinyrss-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_WEKAN_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: wekan-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_WEKAN_PASSWORD
            valueFrom:
              secretKeyRef:
                key: wekan-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_WHITEPAGES_OAUTH2_SECRET
            valueFrom:
              secretKeyRef:
                key: whitepages-oauth2-secret
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_WHITEPAGES_PASSWORD
            valueFrom:
              secretKeyRef:
                key: whitepages-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_WORDPRESS_PASSWORD
            valueFrom:
              secretKeyRef:
                key: wordpress-password
                name: openldap-${FRONTNAME}
          - name: OPENLDAP_WSWEET_PASSWORD
            valueFrom:
              secretKeyRef:
                key: wsweet-password
                name: openldap-${FRONTNAME}
          - name: RESET_SSL
            value: "false"
          - name: TZ
            value: Europe/Paris
          image: ${OPENSHIFT_REGISTRY}/${CI_BUILDS}/openldap:${LDAP_IMAGE_TAG}
          imagePullPolicy: Always
          livenessProbe:
            initialDelaySeconds: 30
            timeoutSeconds: 1
            tcpSocket:
              port: 1389
          name: openldap
          ports:
          - containerPort: 1389
            protocol: TCP
          - containerPort: 1636
            protocol: TCP
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - "-i"
              - "-c"
              - /usr/local/bin/is-ready
            initialDelaySeconds: 5
            timeoutSeconds: 1
          resources:
            limits:
              cpu: "${OPENLDAP_CPU_LIMIT}"
              memory: "${OPENLDAP_MEMORY_LIMIT}"
          volumeMounts:
          - mountPath: /etc/ldap
            name: data
            subPath: config
          - mountPath: /etc/openldap
            name: data
            subPath: config
          - mountPath: /usr/local/openldap/etc/openldap
            name: data
            subPath: config
          - mountPath: /usr/local/openldap/var/log
            name: data
            subPath: logs
          - mountPath: /var/lib/ldap
            name: data
            subPath: db
          - mountPath: /run
            name: run
        volumes:
        - emptyDir: {}
          name: run
    volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        accessModes: [ ReadWriteOnce ]
        resources:
          requests:
            storage: ${OPENLDAP_DATA_VOLUME_CAPACITY}
    triggers:
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: openldap-${FRONTNAME}
  spec:
    ports:
    - name: ldap
      port: 1389
    - name: ldaps
      port: 1636
    selector:
      name: openldap-${FRONTNAME}
    clusterIP: None
parameters:
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: LDAP_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: OPENLDAP_CPU_LIMIT
  description: Maximum amount of CPU an OpenLDAP container can use
  displayName: OpenLDAP CPU Limit
  required: true
  value: 300m
- name: OPENLDAP_DATA_VOLUME_CAPACITY
  description: Volume space available for OpenLDAP database, e.g. 512Mi, 2Gi.
  displayName: OpenLDAP Data Volume Capacity
  required: true
  value: 8Gi
- name: OPENLDAP_DEBUG_LEVEL
  description: OpenLDAP log level
  displayName: LDAP Log Level
  required: true
  value: '256'
- name: OPENLDAP_MEMORY_LIMIT
  description: Maximum amount of memory an OpenLDAP container can use
  displayName: OpenLDAP Memory Limit
  required: true
  value: 512Mi
- name: OPENSHIFT_REGISTRY
  description: OpenShift Registry
  displayName: Registry Address
  required: true
  value: "docker-registry.default.svc:5000"
- name: CI_BUILDS
  description: CI Namespace
  displayName: Builds
  required: true
  value: wsweet-ci
- name: ORG_NAME
  description: Organization Display Name
  displayName: Organization Display Name
  required: true
  value: Demo
- name: ROOT_DOMAIN
  description: Root Domain
  displayName: Root Domain
  required: true
  value: demo.local
- name: SMTP_RELAY
  description: SMTP Relay
  displayName: SMTP Relay
  required: true
  value: smtp.demo.local
