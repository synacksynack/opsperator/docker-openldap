FRONTNAME=opsperator
GITDIR=$(shell pwd)
IMAGE=opsperator/openldap
SKIP_SQUASH?=1
USERID=$(shell id -u)

ifeq ($(TARGET),rhel7)
	OS := rhel7
	TGT := $(TARGET)
else
ifeq ($(TARGET),el7)
	OS := centos7
	TGT := $(TARGET)
else
	OS := debian10
	TGT := deb10
endif
endif
ifeq ($(FLAVOR),distro)
	SRC := distro
ifeq ($(TARGET),rhel7)
	LCONF := /etc/openldap
else
ifeq ($(TARGET),el7)
	LCONF := /etc/openldap
else
	LCONF := /etc/ldap
endif
endif
else
ifeq ($(LTBSRC),true)
	SRC := ltbsrc
else
	SRC := ltb
endif
	LCONF := /usr/local/openldap/etc/openldap
endif

-include Makefile.cust

#################
## Local Runtime
#################
.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh $(OS) $(SRC)

.PHONY: demo
demo: tlsdemo

.PHONY: plaindemo
plaindemo:
	@@docker run \
	    --user $(USERID) \
	    --tmpfs /var:rw,size=65536k \
	    --tmpfs /var/run:rw,size=65536k  \
	    --tmpfs /run/ldap:rw,size=65536k  \
	    --tmpfs $(LCONF):rw,size=65536k \
	    --tmpfs /var/lib/ldap:rw,size=65536k \
	    -e OPENLDAP_GLOBAL_ADMIN_PASSWORD=secret \
	    -e OPENLDAP_DEBUG=yesplease \
	    -e DEBUG=yesplease \
	    -e DO_EXPORTERS=true \
	    -e DO_LEMON=true \
	    -e DO_NEXTCLOUD=true \
	    -e DO_SSP=true \
	    -e DO_SERVICEDESK=true \
	    -e DO_WHITEPAGES=true \
	    -e OPENLDAP_BIND_LDAP_PORT=1389 \
	    -e OPENLDAP_BIND_LDAPS_PORT=1636 \
	    -e OPENLDAP_INIT_DEBUG_LEVEL=256 \
	    -p 1389:1389 -p 1636:1636 \
	    $(IMAGE)

.PHONY: restore
restore:
	@@if test -s "$(GITDIR)/provision/init.ldif"; then \
	    MAINDEV=`ip r | awk '/default/{print $$0;exit;}' | sed 's|.* dev \([^ ]*\).*|\1|'`; \
	    MAINIP=`ip r | awk "/ dev $$MAINDEV .* src /" | sed 's|.* src \([^ ]*\).*$$|\1|'`; \
	    docker run -e OPENLDAP_HOST_ENDPOINT=$$MAINIP \
		--user $(USERID) \
		--tmpfs /var:rw,size=65536k \
		--tmpfs /var/run:rw,size=65536k \
		--tmpfs /run/ldap:rw,size=65536k \
		--tmpfs $(LCONF):rw,size=65536k \
		--tmpfs /var/lib/ldap:rw,size=512M \
		-v "$(GITDIR)/provision:/provision" \
		-e OPENLDAP_GLOBAL_ADMIN_PASSWORD=secret \
		-e OPENLDAP_BIND_LDAP_PORT=1389 \
		-e OPENLDAP_BIND_LDAPS_PORT=1636 \
		-e DEBUG=required-to-detect-directory-root \
		-p 1389:1389 -p 1636:1636 \
		$(IMAGE); \
	else \
	    echo "Can't import data from $(GITDIR)/provision"; \
	    echo "Folder should exist, and contain some init.ldif file"; \
	fi

.PHONY: starttest
starttest:
	@@docker rm -f testldap || true
	@@docker run \
	    --name testldap \
	    --tmpfs /var:rw,size=65536k \
	    --tmpfs /var/run:rw,size=65536k  \
	    --tmpfs /run/ldap:rw,size=65536k  \
	    --tmpfs /usr/local/openldap/etc/openldap:rw,size=65536k \
	    --tmpfs /var/lib/ldap:rw,size=65536k \
	    -e OPENLDAP_GLOBAL_ADMIN_PASSWORD=secret \
	    -e OPENLDAP_DEBUG=yesplease \
	    -e DEBUG=yesplease \
	    -e DO_EXPORTERS=true \
	    -e DO_LEMON=true \
	    -e DO_NEXTCLOUD=true \
	    -e DO_SSP=true \
	    -e DO_SERVICEDESK=true \
	    -e DO_WHITEPAGES=true \
	    -e OPENLDAP_BIND_LDAP_PORT=1389 \
	    -e OPENLDAP_BIND_LDAPS_PORT=1636 \
	    -e OPENLDAP_INIT_DEBUG_LEVEL=256 \
	    -p 1389:1389 -p 1636:1636 \
	    -d $(IMAGE)

.PHONY: test
test: starttest
	@@set -x; \
	sleep 10; \
	works=false; \
	for try in 1 2 3; \
	do \
	    echo "=== Test $$try/3 ==="; \
	    if docker exec testldap /bin/sh -c '/usr/local/bin/is-ready && echo OK' 2>&1 | grep OK; then \
		works=true; \
		break; \
	    elif test $$try -eq 3; then \
		break; \
	    fi; \
	    echo retrying in 5 seconds; \
	    sleep 5; \
	done; \
	if $$works; then \
	    echo "Test: OK, OpenLDAP starts and listens"; \
	else \
	    echo "Test: KO, OpenLDAP does not answer"; \
	    exit 1; \
	fi \

.PHONY: tlsdemo
tlsdemo: ldapcrtgen
	if test -s ./certs/ca.crt -a -s ./certs/tls.crt \
		-a -s ./certs/tls.key; then \
	    docker run \
	        --user $(USERID):1 \
		--tmpfs /var:rw,size=65536k,uid=$(USERID),gid=1 \
		--tmpfs /var/run:rw,size=65536k,uid=$(USERID),gid=1  \
		--tmpfs /run/ldap:rw,size=65536k,uid=$(USERID),gid=1  \
		--tmpfs $(LCONF):rw,size=65536k,uid=$(USERID),gid=1 \
		--tmpfs /var/lib/ldap:rw,size=65536k,uid=$(USERID),gid=1 \
		--tmpfs /usr/local/openldap/etc/openldap:rw,size=65536k,uid=$(USERID),gid=1 \
		--tmpfs /usr/local/openldap/var/log:rw,size=65536k,uid=$(USERID),gid=1 \
		--tmpfs /usr/local/share/ca-certificates:rw,size=65536k,uid=$(USERID),gid=1 \
		--tmpfs /usr/share/ca-certificates:rw,size=65536k,uid=$(USERID),gid=1 \
		-v "$(GITDIR)/certs:/certs" \
		-e OPENLDAP_GLOBAL_ADMIN_PASSWORD=secret \
		-e OPENLDAP_DEBUG=yesplease \
		-e DEBUG=yesplease \
		-e DO_EXPORTERS=true \
		-e DO_LEMON=true \
		-e DO_NEXTCLOUD=true \
		-e DO_SSP=true \
		-e DO_SERVICEDESK=true \
		-e DO_WHITEPAGES=true \
		-e OPENLDAP_BIND_LDAP_PORT=1389 \
		-e OPENLDAP_BIND_LDAPS_PORT=1636 \
		-e OPENLDAP_INIT_DEBUG_LEVEL=256 \
		-p 1389:1389 -p 1636:1636 \
		$(IMAGE); \
	else \
	    echo "Can't load certificates from $(GITDIR)/certs"; \
	    echo "Folder should exist, and contain tls.crt/tls.key/ca.crt"; \
	fi


#################
## K8S Runtime
#################
.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service statefulset; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done


#################
## OKD Runtime
#################
.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "LDAP_BASE=$(TGT)" \
		-p "LDAP_FLAVOR=$(SRC)" \
		-p "LDAP_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "LDAP_BASE=$(TGT)" \
		-p "LDAP_FLAVOR=$(SRC)" \
		-p "LDAP_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocprod
ocprod: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ha.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true

.PHONY: samlcrtgen
samlcrtgen:
	@@if ! test -s /tmp/saml.crt -a -s /tmp/saml.key; then \
	    if ! openssl req -nodes -new -x509 -days 3650 \
		    -keyout /tmp/saml.key -out /tmp/saml.crt -subj /CN=lemon; then \
		echo Failed generarting SAML key pair; \
		exit 1; \
	    fi; \
	fi
	@@if ! ./config/getrsa; then \
	    echo Failed loading SAML certificate data; \
	    exit 1; \
	fi
	@@rm -f /tmp/saml.crt /tmp/saml.key

.PHONY: ldapcrtgen
ldapcrtgen:
	@@test -d ./certs || mkdir -p certs
	@@if ! test -s ./certs/rootCA.key; then \
	    if ! openssl genrsa -out ./certs/rootCA.key 4096; then \
		echo Failed creating CA key; \
		exit 1; \
	    fi; \
	fi
	@@if ! test -s ./certs/ca.crt; then \
	    if ! openssl req -x509 -new -nodes -key ./certs/rootCA.key \
		    -subj "/C=FR/ST=Paris/O=KubeLDAP/CN=CA" \
		    -sha256 -days 1024 -out ./certs/ca.crt; then \
		echo Failed creating CA certificate; \
		exit 1; \
	    fi; \
	fi
	@@if ! test -s ./certs/tls.key; then \
	    if ! openssl genrsa -out ./certs/tls.key 4096; then \
		echo Failed creating server key; \
		exit 1; \
	    fi; \
	fi
	@@if ! test -s ./certs/tls.csr; then \
	    if ! openssl req -new -sha256 -key ./certs/tls.key \
		    -subj "/C=FR/ST=Paris/O=KubeLDAP/CN=openldap" \
		    -out ./certs/tls.csr; then \
		echo Failed creating server csr; \
		exit 1; \
	    fi; \
	fi
	@@if ! test -s ./certs/tls.crt; then \
	    if ! openssl x509 -req -in ./certs/tls.csr -CA ./certs/ca.crt \
		    -CAkey ./certs/rootCA.key -CAcreateserial \
		    -out ./certs/tls.crt -days 500 -sha256; then \
		echo Failed creating server crt; \
		exit 1; \
	    fi; \
	    rm -f .srl; \
	fi
