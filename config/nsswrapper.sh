#!/bin/sh

if test -s /tmp/slapd-passwd; then
    echo Skipping nsswrapper setup - already initialized
elif test "`id -u`" -ne 0; then
    echo Setting up nsswrapper mapping `id -u` to openldap
    sed "s|^openldap:.*|openldap:x:`id -g`:|" /etc/group >/tmp/slapd-group
    sed \
	"s|^openldap:.*|openldap:x:`id -u`:`id -g`:openldap:/var/lib/ldap:/usr/sbin/nologin|" \
	/etc/passwd >/tmp/slapd-passwd
    export NSS_WRAPPER_PASSWD=/tmp/slapd-passwd
    export NSS_WRAPPER_GROUP=/tmp/slapd-group
    if test -s /usr/lib64/libnss_wrapper.so; then
	export LD_PRELOAD=/usr/lib64/libnss_wrapper.so
    else
	export LD_PRELOAD=/usr/lib/libnss_wrapper.so
    fi
fi
