#!/bin/sh

##################################
############ LLNG Helpers Defaults
##################################

if test -z "$OPENLDAP_CONF_DIR" -o -z "$sfx"; then
    . /usr/src/openldap/db-env-init
fi

DID_CLONE=false
LADDR="ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/"
LBCFG="ou=lemonldap,ou=config,$OPENLDAP_ROOT_DN_SUFFIX"
LBRU="cn=syncrepl,ou=services,$OPENLDAP_ROOT_DN_SUFFIX"
LBRPW="$OPENLDAP_SYNCREPL_PASSWORD"
LBWU="cn=lemonldap,ou=services,$OPENLDAP_ROOT_DN_SUFFIX"
LBWPW="$OPENLDAP_LEMONLDAP_PASSWORD"


##################################
###### LLNG Helpers Initialization
##################################

LAST_ID=$(ldapsearch -x -H $LADDR -D "$LBRU" -w "$LBRPW" -b "$LBCFG" \
	    "(objectClass=applicationProcess)" cn \
	    | awk '/^dn: /' \
	    | sed 's/dn: cn=lmConf-\([0-9]*\),ou.*/\1/' \
	    | sort -n | tail -1)
NEXT_ID=$(expr $LAST_ID + 1)

if ldapsearch -x -H $LADDR -D "$LBRU" -w "$LBRPW" -b "$LBCFG" \
	"(&(objectClass=applicationProcess)(cn=lmConf-$LAST_ID))" \
	2>/dev/null | grep "^description: {trustedProxies}" \
	>/dev/null; then
    HAS_LLNG=1.x
else
    CHECK=$(ldapsearch -x -H $LADDR -D "$LBRU" -w "$LBRPW" -b "$LBCFG" \
		"(&(objectClass=applicationProcess)(cn=lmConf-$LAST_ID))" \
		| awk -F} '/^description: {cfgVersion}/{print $2}')
    if test "$CHECK"; then
	HAS_LLNG=$CHECK
    fi
fi


##################################
########### LLNG Helpers Functions
##################################

cloneLmConf()
{
    echo "INFO: preparing new lmConf ($LAST_ID -> $NEXT_ID)"
    if ! ldapsearch -x -H $LADDR -D "$LBRU" -w "$LBRPW" -b "$LBCFG" \
	    "(&(objectClass=applicationProcess)(cn=lmConf-$LAST_ID))" \
	    | grep -vE '^(#|(search|result):|$)' \
	    | sed -e "s|lmConf-$LAST_ID|lmConf-$NEXT_ID|g" \
		  -e "s|{cfgNum}$LAST_ID|{cfgNum}$NEXT_ID|" \
	    | ldapadd -x -H $LADDR -D "$LBWU" -w "$LBWPW"; then
    echo WARNING: failed inserting lmConf-$NEXT_ID >&2
	return 1
    fi
    return 0
}

errExit()
{
    echo "CRITICAL: $@"
    exit 1
}

renderSessionOptions()
{
    local addpw

    if test "$LLNG_SESSIONS_BACKEND" = redis; then
	export sessionmod=Apache::Session::Browseable::Redis
	addpw=
	if test "$LLNG_REDIS_SESSIONS_PASSWORD"; then
	    addpw=",\"password\",\"$LLNG_REDIS_SESSIONS_PASSWORD\""
	fi
	if test "$LLNG_SENTINEL_SESSIONS_HOST"; then
	    if test "$LLNG_SENTINEL_SESSIONS_REPLICASET"; then
		cname=$LLNG_SENTINEL_SESSIONS_REPLICASET
	    else
		cname=`echo $LLNG_SENTINEL_SESSIONS_HOST | cut -d. -f1`
	    fi
	    cstr="\"sentinels\":\"$LLNG_SENTINEL_SESSIONS_HOST:$LLNG_SENTINEL_SESSIONS_PORT\",\"service\":\"$cname\""
	    unset cname
	else
	    cstr="\"server\":\"$LLNG_REDIS_SESSIONS_HOST:$LLNG_REDIS_SESSIONS_PORT\""
	fi
	export casopts="{\"Index\":\"pgtIou _cas_id\",$cstr$addpw,\"select\":\"$LLNG_REDIS_SESSIONS_DATABASE\"}"
	export globalopts="{\"generateModule\":\"Lemonldap::NG::Common::Apache::Session::Generate::SHA256\",\"Index\":\"ipAddr _whatToTrace user\",$cstr$addpw,\"select\":\"$LLNG_REDIS_SESSIONS_DATABASE\"}"
	export lverif=require
	export oidcopts="{\"Index\":\"_session_kind _utime\",$cstr$addpw,\"select\":\"$LLNG_REDIS_SESSIONS_DATABASE\"}"
	export prstopts="{\"Index\":\"ipAddr _whatToTrace user\",$cstr$addpw,\"select\":\"$LLNG_REDIS_SESSIONS_DATABASE\"}"
	export samlopts="{\"Index\":\"ipAddr _whatToTrace user\",$cstr$addpw,\"select\":\"$LLNG_REDIS_SESSIONS_DATABASE\"}"
	unset addpw
    elif test "$LLNG_SESSIONS_BACKEND" = postgres; then
	export sessionmod=Apache::Session::Browseable::Postgres
	cds="\"DataSource\":\"dbi:Pg:dbname=$LLNG_POSTGRES_SESSIONS_DATABASE;host=$LLNG_POSTGRES_SESSIONS_HOST;port=$LLNG_POSTGRES_SESSIONS_PORT\";sslmode=$LLNG_POSTGRES_SESSIONS_SSL"
	cauth="\"UserName\":\"$LLNG_POSTGRES_SESSIONS_USER\",\"Password\":\"$LLNG_POSTGRES_SESSIONS_PASSWORD\""
	cmisc="\"Commit\":\"1\",\"TableName\":\"$LLNG_POSTGRES_SESSIONS_TABLE\""
	cstr="$cds,$cauth,$cmisc"
	export casopts="{\"Index\":\"pgtIou _cas_id\",$cstr}"
	export globalopts="{\"generateModule\":\"Lemonldap::NG::Common::Apache::Session::Generate::SHA256\",\"Index\":\"ipAddr _whatToTrace user\",$cstr}"
	export lverif=require
	export oidcopts="{\"Index\":\"_session_kind _utime\",$cstr}"
	export prstopts="{\"Index\":\"ipAddr _whatToTrace user\",$cstr}"
	export samlopts="{\"Index\":\"ipAddr _whatToTrace user\",$cstr}"
	unset cds cauth cmisc
    else
	export sessionmod=Apache::Session::Browseable::LDAP
	if test "$TLS_EVERYWHERE" = true; then
	    export lverif=require
	    cstr="\"ldapServer\":\"ldaps://$OPENLDAP_HOST_ENDPOINT:$OPENLDAP_BIND_LDAPS_PORT\",\"ldapCAFile\":\"$CAFILE\""
	else
	    cstr="\"ldapServer\":\"ldap://$OPENLDAP_HOST_ENDPOINT:$OPENLDAP_BIND_LDAP_PORT\""
	    export lverif=never
	fi
	export casopts="{\"Index\":\"pgtIou _cas_id\",$cstr,\"ldapConfBase\":\"ou=cas,ou=sessions,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindDN\":\"cn=lemonsessions,ou=services,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindPassword\":\"$OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD\",\"ldapRaw\":\"(?i:^jpegPhoto|;binary)\"}"
	export globalopts="{\"generateModule\":\"Lemonldap::NG::Common::Apache::Session::Generate::SHA256\",\"Index\":\"ipAddr _whatToTrace user\",$cstr,\"ldapConfBase\":\"ou=global,ou=sessions,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindDN\":\"cn=lemonsessions,ou=services,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindPassword\":\"$OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD\",\"ldapRaw\":\"(?i:^jpegPhoto|;binary)\"}"
	export oidcopts="{\"Index\":\"_session_kind _utime\",$cstr,\"ldapConfBase\":\"ou=oidc,ou=sessions,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindDN\":\"cn=lemonsessions,ou=services,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindPassword\":\"$OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD\",\"ldapRaw\":\"(?i:^jpegPhoto|;binary)\"}"
	export prstopts="{\"Index\":\"ipAddr _whatToTrace user\",$cstr,\"ldapConfBase\":\"ou=persistent,ou=sessions,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindDN\":\"cn=lemonsessions,ou=services,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindPassword\":\"$OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD\"}"
	export samlopts="{\"Index\":\"ipAddr _whatToTrace user\",$cstr,\"ldapConfBase\":\"ou=saml,ou=sessions,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindDN\":\"cn=lemonsessions,ou=services,$OPENLDAP_ROOT_DN_SUFFIX\",\"ldapBindPassword\":\"$OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD\",\"ldapRaw\":\"(?i:^jpegPhoto|;binary)\"}"
    fi
    unset cstr
}

renderVhostOptions()
{
    local itemid

    itemid=1
    applist="{\"$(renderItemId $itemid)-cat\":{\"catname\":\"$PORTAL_CAT_MAIN\",\"type\":\"category\""
    exportedhdrs="{\"auth.$OPENLDAP_ROOT_DOMAIN\":{},\"manager.$OPENLDAP_ROOT_DOMAIN\":{}"
    ldapvars='{"userPassword":"userPassword","cloudQuota":"cloudQuota","nextCloudGSSBackend":"nextCloudGSSBackend","mail":"mail","uidNumber":"uidNumber","uid":"uid","mailQuota":"mailQuota"}'
    locationrules="{\"manager.$OPENLDAP_ROOT_DOMAIN\":{\"default\":\"\$hGroups->{'Admins'}\"}"
    casapphdrs="{"
    casappmeta="{"
    oidcrpattrs="{"
    oidcrpmeta="{"
    oidcrpxtrattrs="{"
    post="{\"manager.$OPENLDAP_ROOT_DOMAIN\":{},\"auth.$OPENLDAP_ROOT_DOMAIN\":{}"
    samlattrs="{"
    samlmeta="{"
    samlxml="{"

    if test "$DEBUG"; then
	loglevel=debug
    else
	loglevel=info
    fi
    vhostopts="{\"manager.$OPENLDAP_ROOT_DOMAIN\":{},\"auth.$OPENLDAP_ROOT_DOMAIN\":{}"
    itemid=$(expr $itemid + 1)
    if test "$DO_BM" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"BlueMind\",\"logo\":\"bluemind.png\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MAIL_DOMAIN\",\"description\":\"$BM_DESC\"}}"
	casapphdrs="$casapphdrs,\"$MAIL_DOMAIN\":{\"mail\":\"mail\",\"uid\":\"uid\",\"cn\":\"cn\",\"mailQuota\":\"mailQuota\"}"
	casappmeta="$casappmeta,\"$MAIL_DOMAIN\":{\"casAppMetaDataOptionsUserAttribute\":\"uid\",\"casAppMetaDataOptionsService\":\"https://$MAIL_DOMAIN/\"}"
	exportedhdrs="$exportedhdrs,\"$MAIL_DOMAIN\":{}"
	locationrules="$locationrules,\"$MAIL_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$MAIL_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MAIL_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_CODIMD" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"CodiMD\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$CD_DOMAIN\",\"description\":\"$CD_DESC\",\"logo\":\"codimd.png\"}}"
	exportedhdrs="$exportedhdrs,\"$CD_DOMAIN\":{}"
	locationrules="$locationrules,\"$CD_DOMAIN\":{\"^/css\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$CD_DOMAIN\":{}"
	samlattrs="$samlattrs,\"codimd\":{\"uid\":\"1;uid\",\"mail\":\"1;mail\"}"
	samlmeta="$samlmeta,\"codimd\":{\"samlSPMetaDataOptionsSignSLOMessage\":-1,\"samlSPMetaDataOptionsEnableIDPInitiatedURL\":0,\"samlSPMetaDataOptionsNameIDFormat\":\"\",\"samlSPMetaDataOptionsEncryptionMode\":\"none\",\"samlSPMetaDataOptionsSessionNotOnOrAfterTimeout\":72000,\"samlSPMetaDataOptionsSignSSOMessage\":-1,\"samlSPMetaDataOptionsCheckSSOMessageSignature\":0,\"samlSPMetaDataOptionsForceUTF8\":1,\"samlSPMetaDataOptionsCheckSLOMessageSignature\":0,\"samlSPMetaDataOptionsOneTimeUse\":0,\"samlSPMetaDataOptionsNotOnOrAfterTimeout\":72000}"
	samlxml="$samlxml,\"codimd\":{\"samlSPMetaDataXML\":\"<?xml version=\\\"1.0\\\"?><EntityDescriptor xmlns=\\\"urn:oasis:names:tc:SAML:2.0:metadata\\\" xmlns:ds=\\\"http://www.w3.org/2000/09/xmldsig#\\\" entityID=\\\"$OPENLDAP_LEMONLDAP_PROTO://$CD_DOMAIN\\\" ID=\\\"${OPENLDAP_LEMONLDAP_PROTO}___codimd_$ORG_SHORT\\\"><SPSSODescriptor protocolSupportEnumeration=\\\"urn:oasis:names:tc:SAML:2.0:protocol\\\"><NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</NameIDFormat><AssertionConsumerService index=\\\"1\\\" isDefault=\\\"true\\\" Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$CD_DOMAIN/auth/saml/callback\\\"/></SPSSODescriptor></EntityDescriptor>\"}"
	vhostopts="$vhostopts,\"$CD_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_DIASPORA" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Diaspora\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$DS_DOMAIN\",\"description\":\"$DS_DESC\",\"logo\":\"diaspora.png\"}}"
	exportedhdrs="$exportedhdrs,\"$DS_DOMAIN\":{}"
	locationrules="$locationrules,\"$DS_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$DS_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$DS_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_DOKUWIKI" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"DokuWiki\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$DK_DOMAIN\",\"description\":\"$DK_DESC\",\"logo\":\"dokuwiki.png\"}}"
	exportedhdrs="$exportedhdrs,\"$DK_DOMAIN\":{\"Auth-User\":\"\$uid\",\"Auth-Cn\":\"\$cn\",\"Auth-Mail\":\"\$mail\",\"Auth-Groups\":\"encode_base64(\$groups,'')\"}"
	locationrules="$locationrules,\"$DK_DOMAIN\":{\"^/lib/styles/\":\"skip\",\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$DK_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$DK_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_DRAW" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Draw\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$DR_DOMAIN\",\"description\":\"$DR_DESC\",\"logo\":\"draw.png\"}}"
	exportedhdrs="$exportedhdrs,\"$DR_DOMAIN\":{}"
	locationrules="$locationrules,\"$DR_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"draw\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"draw\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_DRAW_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"draw\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$DR_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"draw\":{}"
	post="$post,\"$DR_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$DR_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ETHERPAD" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"EtherPad\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$EP_DOMAIN\",\"description\":\"$EP_DESC\",\"logo\":\"etherpad.png\"}}"
	exportedhdrs="$exportedhdrs,\"$EP_DOMAIN\":{},\"blue-$EP_DOMAIN\":{},\"green-$EP_DOMAIN\":{}"
	locationrules="$locationrules,\"$EP_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"accept\"},\"green-$EP_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"accept\"},\"blue-$EP_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"accept\"}"
	post="$post,\"$EP_DOMAIN\":{},\"green-$EP_DOMAIN\":{},\"blue-$EP_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$EP_DOMAIN\":{},\"blue-$EP_DOMAIN\":{},\"green-$EP_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ETHERCALC" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"EtherCalc\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$EC_DOMAIN\",\"description\":\"$EC_DESC\",\"logo\":\"ethercalc.png\"}}"
	exportedhdrs="$exportedhdrs,\"$EC_DOMAIN\":{},\"blue-$EC_DOMAIN\":{},\"green-$EC_DOMAIN\":{}"
	locationrules="$locationrules,\"$EC_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"default\":\"accept\"},\"green-$EC_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"accept\"},\"blue-$EC_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"accept\"}"
	post="$post,\"$EC_DOMAIN\":{},\"green-$EC_DOMAIN\":{},\"blue-$EC_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$EC_DOMAIN\":{},\"blue-$EC_DOMAIN\":{},\"green-$EC_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_GREENLIGHT" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"logo\":\"bigbluebutton.png\",\"name\":\"Greenlight\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$BBB_DOMAIN/b/signin\",\"description\":\"$GL_DESC\"}}"
	exportedhdrs="$exportedhdrs,\"$BBB_DOMAIN\":{}"
	locationrules="$locationrules,\"$BBB_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$BBB_DOMAIN\":{}"
	oidcrpattrs="$oidcrpattrs,\"greenlight\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"greenlight\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"HS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_GREENLIGHT_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"greenlight\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$BBB_DOMAIN/b/auth/openid_connect/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"greenlight\":{}"
	samlattrs="$samlattrs,\"greenlight\":{\"uid\":\"1;uid\",\"mail\":\"1;mail\"}"
	samlmeta="$samlmeta,\"greenlight\":{}"
	samlxml="$samlxml,\"greenlight\":{\"samlSPMetaDataXML\":\"<md:EntityDescriptor xmlns:md=\\\"urn:oasis:names:tc:SAML:2.0:metadata\\\" xmlns:saml=\\\"urn:oasis:names:tc:SAML:2.0:assertion\\\" entityID=\\\"greenlight\\\"><md:SPSSODescriptor AuthnRequestsSigned=\\\"false\\\" WantAssertionsSigned=\\\"false\\\" protocolSupportEnumeration=\\\"urn:oasis:names:tc:SAML:2.0:protocol\\\"><md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</md:NameIDFormat><md:AssertionConsumerService Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$BBB_DOMAIN/b/auth/saml/callback\\\" index=\\\"0\\\" isDefault=\\\"true\\\"/><md:AttributeConsumingService index=\\\"1\\\" isDefault=\\\"true\\\"><md:ServiceName xml:lang=\\\"en\\\">Required attributes</md:ServiceName><md:RequestedAttribute FriendlyName=\\\"Email address\\\" Name=\\\"email\\\" NameFormat=\\\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\\\" isRequired=\\\"false\\\"/><md:RequestedAttribute FriendlyName=\\\"Full name\\\" Name=\\\"name\\\" NameFormat=\\\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\\\" isRequired=\\\"false\\\"/><md:RequestedAttribute FriendlyName=\\\"Given name\\\" Name=\\\"first_name\\\" NameFormat=\\\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\\\" isRequired=\\\"false\\\"/><md:RequestedAttribute FriendlyName=\\\"Family name\\\" Name=\\\"last_name\\\" NameFormat=\\\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\\\" isRequired=\\\"false\\\"/></md:AttributeConsumingService></md:SPSSODescriptor></md:EntityDescriptor>\"}"
	vhostopts="$vhostopts,\"$BBB_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_JITSI" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Jitsi\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$JM_DOMAIN\",\"description\":\"$JM_DESC\",\"logo\":\"jitsi.png\"}}"
	exportedhdrs="$exportedhdrs,\"$JM_DOMAIN\":{}"
	locationrules="$locationrules,\"$JM_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$JM_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$JM_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_KIWIIRC" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Kiwi IRC\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$KW_DOMAIN\",\"description\":\"$KW_DESC\",\"logo\":\"kiwiirc.png\"}}"
	exportedhdrs="$exportedhdrs,\"$KW_DOMAIN\":{}"
	locationrules="$locationrules,\"$KW_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"kiwiirc\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"kiwiirc\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_KIWIIRC_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"kiwiirc\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$KW_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"kiwiirc\":{}"
	post="$post,\"$KW_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$KW_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MASTODON" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Mastodon\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MO_DOMAIN\",\"description\":\"$MO_DESC\",\"logo\":\"mastodon.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MO_DOMAIN\":{}"
	locationrules="$locationrules,\"$MO_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$MO_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MO_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MATOMO" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Matomo\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MT_DOMAIN\",\"description\":\"$MT_DESC\",\"logo\":\"matomo.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MT_DOMAIN\":{}"
	locationrules="$locationrules,\"$MT_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"matomo\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"matomo\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"HS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_MATOMO_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"matomo\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$MT_DOMAIN/index.php?module=LoginOIDC&action=callback&provider=oidc\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"matomo\":{}"
	post="$post,\"$MT_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MT_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MATTERMOST" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Mattermost\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MM_DOMAIN\",\"description\":\"$MM_DESC\",\"logo\":\"mattermost.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MM_DOMAIN\":{}"
	locationrules="$locationrules,\"$MM_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"mattermost\":{\"name\":\"uid\",\"email\":\"mail\",\"id\":\"uidNumberCast\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"mattermost\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"HS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_MATTERMOST_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"mattermost\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$MM_DOMAIN/signup/gitlab/complete\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"mattermost\":{\"gitlab\":\"id username name email\"}"
	post="$post,\"$MM_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MM_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_NEXTCLOUD" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"description\":\"$NC_DESC\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$NC_DOMAIN\",\"display\":\"auto\",\"name\":\"Nextcloud\",\"logo\":\"nextcloud.png\"}}"
	exportedhdrs="$exportedhdrs,\"$NC_DOMAIN\":{}"
	locationrules="$locationrules,\"$NC_DOMAIN\":{\"^/ocs/v2.php/apps/serverinfo/api/v1/info\":\"skip\",\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$NC_DOMAIN\":{}"
	samlattrs="$samlattrs,\"nextcloud\":{\"uid\":\"1;uid\",\"mail\":\"1;mail\",\"cloudQuota\":\"1;cloudQuota;;\",\"nc-gss-backend\":\"0;nextCloudGSSBackend;;\"}"
	samlmeta="$samlmeta,\"nextcloud\":{\"samlSPMetaDataOptionsSignSLOMessage\":-1,\"samlSPMetaDataOptionsEnableIDPInitiatedURL\":0,\"samlSPMetaDataOptionsNameIDFormat\":\"\",\"samlSPMetaDataOptionsEncryptionMode\":\"none\",\"samlSPMetaDataOptionsSessionNotOnOrAfterTimeout\":72000,\"samlSPMetaDataOptionsSignSSOMessage\":-1,\"samlSPMetaDataOptionsCheckSSOMessageSignature\":0,\"samlSPMetaDataOptionsForceUTF8\":1,\"samlSPMetaDataOptionsCheckSLOMessageSignature\":0,\"samlSPMetaDataOptionsOneTimeUse\":0,\"samlSPMetaDataOptionsNotOnOrAfterTimeout\":72000}"
	samlxml="$samlxml,\"nextcloud\":{\"samlSPMetaDataXML\":\"<?xml version=\\\"1.0\\\"?><EntityDescriptor xmlns=\\\"urn:oasis:names:tc:SAML:2.0:metadata\\\" validUntil=\\\"2018-07-10T10:28:25Z\\\" cacheDuration=\\\"PT604800S\\\" entityID=\\\"$OPENLDAP_LEMONLDAP_PROTO://$NC_DOMAIN/index.php/apps/user_saml/saml/metadata\\\"><SPSSODescriptor AuthnRequestsSigned=\\\"false\\\" WantAssertionsSigned=\\\"false\\\" protocolSupportEnumeration=\\\"urn:oasis:names:tc:SAML:2.0:protocol\\\"><SingleLogoutService Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$NC_DOMAIN/index.php/apps/user_saml/saml/sls\\\" /><NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified</NameIDFormat><AssertionConsumerService Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$NC_DOMAIN/index.php/apps/user_saml/saml/acs\\\" index=\\\"1\\\" /></SPSSODescriptor></EntityDescriptor>\"}"
	vhostopts="$vhostopts,\"$NC_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ROCKET" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"logo\":\"rocketchat.png\",\"name\":\"Rocket.Chat\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$RC_DOMAIN\",\"description\":\"$RC_DESC\"}}"
	exportedhdrs="$exportedhdrs,\"$RC_DOMAIN\":{}"
	locationrules="$locationrules,\"$RC_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$RC_DOMAIN\":{}"
	samlattrs="$samlattrs,\"rocket\":{\"uid\":\"1;uid\",\"mail\":\"1;mail\"}"
	samlmeta="$samlmeta,\"rocket\":{}"
	samlxml="$samlxml,\"rocket\":{\"samlSPMetaDataXML\":\"<?xml version=\\\"1.0\\\"?><EntityDescriptor xmlns=\\\"urn:oasis:names:tc:SAML:2.0:metadata\\\" xmlns:ds=\\\"http://www.w3.org/2000/09/xmldsig#\\\" entityID=\\\"$OPENLDAP_LEMONLDAP_PROTO://auth.$OPENLDAP_ROOT_DOMAIN/saml/metadata\\\"><SPSSODescriptor protocolSupportEnumeration=\\\"urn:oasis:names:tc:SAML:2.0:protocol\\\"><SingleLogoutService Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$RC_DOMAIN/_saml/logout/rocket/\\\" ResponseLocation=\\\"$OPENLDAP_LEMONLDAP_PROTO://$RC_DOMAIN/_saml/logout/rocket/\\\"/><NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</NameIDFormat><AssertionConsumerService index=\\\"1\\\" isDefault=\\\"true\\\" Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$RC_DOMAIN/_saml/validate/rocket\\\"/></SPSSODescriptor></EntityDescriptor>\"}"
	vhostopts="$vhostopts,\"$RC_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ROUNDCUBE" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"RoundCube\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$RK_DOMAIN\",\"description\":\"$RK_DESC\",\"logo\":\"roundcube.png\"}}"
	exportedhdrs="$exportedhdrs,\"$RK_DOMAIN\":{}"
	locationrules="$locationrules,\"$RK_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$RK_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$RK_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_SYMPA" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Sympa\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SM_DOMAIN\",\"description\":\"$SM_DESC\",\"logo\":\"sympa.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SM_DOMAIN\":{\"Auth-User\":\"\$uid\",\"Mail\":\"\$mail\"}"
	locationrules="$locationrules,\"$SM_DOMAIN\":{\"^/sympa/sso_login/lemon\":\"\$hGroups->{'SSOUsers'}\",\"default\":\"skip\"}"
	post="$post,\"$SM_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SM_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_SSP" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"SelfServicePassword\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SP_DOMAIN\",\"description\":\"$SP_DESC\",\"logo\":\"ltb.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SP_DOMAIN\":{}"
	locationrules="$locationrules,\"$SP_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"ssp\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"ssp\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_SSP_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"ssp\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$SP_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"ssp\":{}"
	post="$post,\"$SP_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SP_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_SOGO" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"SOGo\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SG_DOMAIN\",\"description\":\"$SG_DESC\",\"logo\":\"sogo.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SG_DOMAIN\":{}"
	locationrules="$locationrules,\"$SG_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$SG_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SG_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_SOPLANNING" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"SOPlanning\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SO_DOMAIN\",\"description\":\"$SO_DESC\",\"logo\":\"soplanning.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SO_DOMAIN\":{}"
	locationrules="$locationrules,\"$SO_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$SO_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SO_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_SYSPASS" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"SysPass\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SY_DOMAIN\",\"description\":\"$SY_DESC\",\"logo\":\"syspass.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SY_DOMAIN\":{}"
	locationrules="$locationrules,\"$SY_DOMAIN\":{\"^/public/js/\":\"skip\",\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$SY_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SY_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_TINYTINYRSS" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"TinyTinyRSS\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$TR_DOMAIN\",\"description\":\"$TR_DESC\",\"logo\":\"tinytinyrss.png\"}}"
	exportedhdrs="$exportedhdrs,\"$TR_DOMAIN\":{}"
	locationrules="$locationrules,\"$TR_DOMAIN\":{\"^/tt-rss/js/\":\"skip\",\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"tinytinyrss\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"tinytinyrss\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_TINYTINYRSS_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"whitepages\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$TR_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"tinytinyrss\":{}"
	post="$post,\"$TR_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$TR_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_WEKAN" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"KanBan\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$WK_DOMAIN\",\"description\":\"$WK_DESC\",\"logo\":\"wekan.png\"}}"
	exportedhdrs="$exportedhdrs,\"$WK_DOMAIN\":{}"
	locationrules="$locationrules,\"$WK_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"wekan\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"wekan\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"HS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_WEKAN_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"wekan\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$WK_DOMAIN/_oauth/oidc?close\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"wekan\":{}"
	post="$post,\"$WK_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$WK_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_WHITEPAGES" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"WhitePages\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$WH_DOMAIN\",\"description\":\"$WH_DESC\",\"logo\":\"ltb.png\"}}"
	exportedhdrs="$exportedhdrs,\"$WH_DOMAIN\":{}"
	locationrules="$locationrules,\"$WH_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"whitepages\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"whitepages\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_WHITEPAGES_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"whitepages\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$WH_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"whitepages\":{}"
	post="$post,\"$WH_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$WH_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_AIRSONIC" = true -o "$DO_MINIO" = true -p "$DO_MINIO_CONSOLE" = true -o "$DO_PEERTUBE" = true -o "$DO_WORDPRESS" = true -o "$DO_MEDUSA" = true -o "$DO_SABNZBD" = true -o "$DO_NEWZNAB" -o "$DO_TRANSMISSION"; then
	applist="${applist}},\"$(renderItemId $itemid)-cat\":{\"catname\":\"$PORTAL_CAT_MEDIA\",\"type\":\"category\""
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_AIRSONIC" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Airsonic\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$AS_DOMAIN\",\"description\":\"$AS_DESC\",\"logo\":\"airsonic.png\"}}"
	exportedhdrs="$exportedhdrs,\"$AS_DOMAIN\":{}"
	locationrules="$locationrules,\"$AS_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$AS_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$AS_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MEDUSA" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Medusa\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MS_DOMAIN\",\"description\":\"$MS_DESC\",\"logo\":\"medusa.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MS_DOMAIN\":{}"
	locationrules="$locationrules,\"$MS_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"medusa\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"medusa\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_MEDUSA_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"medusa\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$MS_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"medusa\":{}"
	post="$post,\"$MS_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MS_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_NEWZNAB" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Newznab\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$NN_DOMAIN\",\"description\":\"$NN_DESC\",\"logo\":\"newznab.png\"}}"
	exportedhdrs="$exportedhdrs,\"$NN_DOMAIN\":{}"
	locationrules="$locationrules,\"$NN_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$NN_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$NN_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MINIO" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"MinIO\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MI_DOMAIN\",\"description\":\"$MI_DESC\",\"logo\":\"minio.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MI_DOMAIN\":{}"
	locationrules="$locationrules,\"$MI_DOMAIN\":{\"default\":\"skip\"}"
	post="$post,\"$MI_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MI_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MINIOCONSOLE" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"MinIO Console\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MC_DOMAIN\",\"description\":\"$MC_DESC\",\"logo\":\"minio.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MC_DOMAIN\":{}"
	locationrules="$locationrules,\"$MC_DOMAIN\":{\"default\":\"skip\"}"
	post="$post,\"$MC_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MC_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_PEERTUBE" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"PeerTube\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$PT_DOMAIN\",\"description\":\"$PT_DESC\",\"logo\":\"peertube.png\"}}"
	exportedhdrs="$exportedhdrs,\"$PT_DOMAIN\":{}"
	locationrules="$locationrules,\"$PT_DOMAIN\":{\"default\":\"skip\"}"
	oidcrpattrs="$oidcrpattrs,\"peertube\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"peertube\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS256\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_PEERTUBE_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"peertube\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$PT_DOMAIN/plugins/auth-openid-connect/router/code-cb\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"peertube\":{}"
	post="$post,\"$PT_DOMAIN\":{}"
	samlattrs="$samlattrs,\"peertube\":{\"uid\":\"1;uid\",\"mail\":\"1;mail\"}"
	samlmeta="$samlmeta,\"peertube\":{}"
	samlxml="$samlxml,\"peertube\":{\"samlSPMetaDataXML\":\"<?xml version=\\\"1.0\\\"?><md:EntityDescriptor xmlns:md=\\\"urn:oasis:names:tc:SAML:2.0:metadata\\\" validUntil=\\\"2017-10-10T19:27:37Z\\\" entityID=\\\"peertubue\\\"><md:SPSSODescriptor protocolSupportEnumeration=\\\"urn:oasis:names:tc:SAML:1.1:protocol urn:oasis:names:tc:SAML:2.0:protocol\\\"> <md:SingleLogoutService Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$PT_DOMAIN/plugins/auth-saml2/router/assert\\\"/> <md:AssertionConsumerService Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$PT_DOMAIN/plugins/auth-saml2/router/assert\\\" index=\\\"0\\\" /></md:SPSSODescriptor></md:EntityDescriptor>\"}"
	vhostopts="$vhostopts,\"$PT_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_SABNZBD" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"SABnzbd\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SB_DOMAIN\",\"description\":\"$SB_DESC\",\"logo\":\"sabnzbd.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SB_DOMAIN\":{}"
	locationrules="$locationrules,\"$SB_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"sabnzbd\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"sabnzbd\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_SABNZBD_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"sabnzbd\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$SB_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"sabnzbd\":{}"
	post="$post,\"$SB_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SB_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"SABnzbd\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SZ_DOMAIN\",\"description\":\"$SZ_DESC\",\"logo\":\"sabnzbd.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SZ_DOMAIN\":{}"
	locationrules="$locationrules,\"$SZ_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"sabnzbdcompleted\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"sabnzbdcompleted\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_SABNZBD_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"sabnzbdcompleted\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$SZ_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"sabnzbdcompleted\":{}"
	post="$post,\"$SZ_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SZ_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_TRANSMISSION" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Transmission\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$TX_DOMAIN\",\"description\":\"$TX_DESC\",\"logo\":\"transmission.png\"}}"
	exportedhdrs="$exportedhdrs,\"$TX_DOMAIN\":{}"
	locationrules="$locationrules,\"$TX_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"transmission\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"transmission\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_TRANSMISSION_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"transmission\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$TX_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"transmission\":{}"
	post="$post,\"$TX_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$TX_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Transmission\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$TZ_DOMAIN\",\"description\":\"$TZ_DESC\",\"logo\":\"transmission.png\"}}"
	exportedhdrs="$exportedhdrs,\"$TZ_DOMAIN\":{}"
	locationrules="$locationrules,\"$TZ_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"transmissioncompleted\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"transmissioncompleted\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_TRANSMISSION_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"transmissioncompleted\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$TZ_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"transmissioncompleted\":{}"
	post="$post,\"$TZ_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$TZ_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_WORDPRESS" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Blog\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$WP_DOMAIN\",\"description\":\"$WP_DESC\",\"logo\":\"wordpress.png\"}}"
	exportedhdrs="$exportedhdrs,\"$WP_DOMAIN\":{}"
	locationrules="$locationrules,\"$WP_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$WP_DOMAIN\":{}"
	samlattrs="$samlattrs,\"wordpress\":{\"uid\":\"1;uid\",\"mail\":\"1;mail\"}"
	samlmeta="$samlmeta,\"wordpress\":{}"
	samlxml="$samlxml,\"wordpress\":{\"samlSPMetaDataXML\":\"<?xml version=\\\"1.0\\\"?><md:EntityDescriptor xmlns:md=\\\"urn:oasis:names:tc:SAML:2.0:metadata\\\" validUntil=\\\"2017-10-10T19:27:37Z\\\" cacheDuration=\\\"PT604800S\\\" entityID=\\\"php-saml\\\"><md:SPSSODescriptor AuthnRequestsSigned=\\\"false\\\" WantAssertionsSigned=\\\"false\\\" protocolSupportEnumeration=\\\"urn:oasis:names:tc:SAML:2.0:protocol\\\"> <md:SingleLogoutService Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$WP_DOMAIN/wp-login.php?saml_sls\\\" /><md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified</md:NameIDFormat><md:AssertionConsumerService Binding=\\\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\\\" Location=\\\"$OPENLDAP_LEMONLDAP_PROTO://$WP_DOMAIN/wp-login.php?saml_acs\\\" index=\\\"1\\\" /></md:SPSSODescriptor></md:EntityDescriptor>\"}"
	vhostopts="$vhostopts,\"$WP_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ARA" = true -o "$DO_ARTIFACTORY" = true -o "$DO_ERRBIT" = true -o "$DO_JENKINS" = true -o "$DO_GERRIT" = true -o \
	    "$DO_GITEA" = true -o "$DO_GOGS" = true -o "$DO_MAILDROP" -o "$DO_MAILHOG" = true -o "$DO_NEXUS" = true -o \
	    "$DO_RELEASEBELL" = true -o "$DO_REGISTRYCONSOLE" -o "$DO_SONARQUBE" = true; then
	applist="${applist}},\"$(renderItemId $itemid)-cat\":{\"catname\":\"$PORTAL_CAT_CI\",\"type\":\"category\""
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ARA" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Ara\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$ARA_DOMAIN\",\"description\":\"$AR_DESC\",\"logo\":\"ara.png\"}}"
	exportedhdrs="$exportedhdrs,\"$ARA_DOMAIN\":{}"
	locationrules="$locationrules,\"$ARA_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"ara\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"ara\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_ARA_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"ara\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$ARA_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"ara\":{}"
	post="$post,\"$ARA_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$ARA_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ARTIFACTORY" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Artifactory\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$AF_DOMAIN\",\"description\":\"$AF_DESC\",\"logo\":\"artifactory.png\"}}"
	exportedhdrs="$exportedhdrs,\"$AF_DOMAIN\":{}"
	locationrules="$locationrules,\"$AF_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$AF_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$AF_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_AWX" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Ansible AWX\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$AX_DOMAIN\",\"description\":\"$AX_DESC\",\"logo\":\"awx.png\"}}"
	exportedhdrs="$exportedhdrs,\"$AX_DOMAIN\":{}"
	locationrules="$locationrules,\"$AX_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$AX_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$AX_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ERRBIT" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Errbit\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$EB_DOMAIN\",\"description\":\"$ER_DESC\",\"logo\":\"errbit.png\"}}"
	exportedhdrs="$exportedhdrs,\"$EB_DOMAIN\":{}"
	locationrules="$locationrules,\"$EB_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"errbit\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"errbit\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_ERRBIT_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"errbit\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$EB_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"errbit\":{}"
	post="$post,\"$EB_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$EB_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_JENKINS" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Jenkins\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$JK_DOMAIN\",\"description\":\"$JK_DESC\",\"logo\":\"jenkins.png\"}}"
	exportedhdrs="$exportedhdrs,\"$JK_DOMAIN\":{}"
	locationrules="$locationrules,\"$JK_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$JK_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$JK_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_GERRIT" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Gerrit\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$GR_DOMAIN\",\"description\":\"$GR_DESC\",\"logo\":\"gerrit.png\"}}"
	exportedhdrs="$exportedhdrs,\"$GR_DOMAIN\":{}"
	locationrules="$locationrules,\"$GR_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$GR_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$GR_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_GITEA" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Gitea\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$GTA_DOMAIN\",\"description\":\"$GT_DESC\",\"logo\":\"gitea.png\"}}"
	exportedhdrs="$exportedhdrs,\"$GTA_DOMAIN\":{}"
	locationrules="$locationrules,\"$GTA_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"gitea\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"gitea\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS256\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_GITEA_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"gitea\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$GTA_DOMAIN/user/oauth2/LLNG/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"gitea\":{}"
	post="$post,\"$GTA_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$GTA_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_GOGS" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Gogs\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$GGS_DOMAIN\",\"description\":\"$GO_DESC\",\"logo\":\"gogs.png\"}}"
	exportedhdrs="$exportedhdrs,\"$GGS_DOMAIN\":{}"
	locationrules="$locationrules,\"$GGS_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$GGS_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$GGS_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MAILDROP" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Maildrop\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MD_DOMAIN\",\"description\":\"$MD_DESC\",\"logo\":\"maildrop.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MD_DOMAIN\":{}"
	locationrules="$locationrules,\"$MD_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$MD_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MD_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MAILHOG" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Mailhog\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MH_DOMAIN\",\"description\":\"$MH_DESC\",\"logo\":\"mailhog.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MH_DOMAIN\":{}"
	locationrules="$locationrules,\"$MH_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$MH_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MH_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_NEXUS" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Nexus\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$NX_DOMAIN\",\"description\":\"$NX_DESC\",\"logo\":\"nexus.png\"}}"
	exportedhdrs="$exportedhdrs,\"$NX_DOMAIN\":{}"
	locationrules="$locationrules,\"$NX_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$NX_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$NX_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_REGISTRYCONSOLE" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Registry Console\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$RG_DOMAIN\",\"description\":\"$RG_DESC\",\"logo\":\"registry.png\"}}"
	exportedhdrs="$exportedhdrs,\"$RG_DOMAIN\":{}"
	locationrules="$locationrules,\"$RG_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"registry\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"registry\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_REGISTRYCONSOLE_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"registry\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$RG_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"registry\":{}"
	post="$post,\"$RG_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$RG_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_RELEASEBELL" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"ReleaseBell\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$RB_DOMAIN\",\"description\":\"$RB_DESC\",\"logo\":\"releasebell.png\"}}"
	exportedhdrs="$exportedhdrs,\"$RB_DOMAIN\":{}"
	locationrules="$locationrules,\"$RB_DOMAIN\":{\"^/public\":\"skip\",\"^/api/v1/status\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"accept\"},\"green-$RB_DOMAIN\":{\"^/public\":\"skip\",\"^/api/v1/status\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"accept\"},\"blue-$RB_DOMAIN\":{\"^/public\":\"skip\",\"^/api/v1/status\":\"skip\",\"^/metrics\":\"skip\",\"default\":\"accept\"}"
	post="$post,\"$RB_DOMAIN\":{},\"green-$RB_DOMAIN\":{},\"blue-$RB_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$RB_DOMAIN\":{},\"blue-$RB_DOMAIN\":{},\"green-$RB_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_SONARQUBE" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"SonarQube\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SQ_DOMAIN\",\"description\":\"$SQ_DESC\",\"logo\":\"sonarqube.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SQ_DOMAIN\":{}"
	locationrules="$locationrules,\"$SQ_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	post="$post,\"$SQ_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SQ_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ESCOMRADE" = true -o "$DO_GRAFANA" = true -o "$DO_KIBANA" = true -o "$DO_PROMETHEUS" = true -o "$DO_ALERTMANAGER" = true -o "$DO_THANOS" = true -o "$DO_MUNIN" = true; then
	applist="${applist}},\"$(renderItemId $itemid)-cat\":{\"catname\":\"$PORTAL_CAT_MONIT\",\"type\":\"category\""
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ALERTMANAGER" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"AlertManager\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$AM_DOMAIN\",\"description\":\"$AM_DESC\",\"logo\":\"alertmanager.png\"}}"
	exportedhdrs="$exportedhdrs,\"$AM_DOMAIN\":{}"
	locationrules="$locationrules,\"$AM_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"alertmanager\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"alertmanager\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_ALERTMANAGER_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"alertmanager\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$AM_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"alertmanager\":{}"
	post="$post,\"$AM_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$AM_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_ESCOMRADE" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Comrade\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$CR_DOMAIN\",\"description\":\"$CR_DESC\",\"logo\":\"elasticsearch.png\"}}"
	exportedhdrs="$exportedhdrs,\"$CR_DOMAIN\":{}"
	locationrules="$locationrules,\"$CR_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"comrade\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"comrade\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS256\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_ESCOMRADE_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"comrade\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$CR_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"comrade\":{}"
	post="$post,\"$CR_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$CR_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_GRAFANA" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Grafana\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$GF_DOMAIN\",\"description\":\"$GF_DESC\",\"logo\":\"grafana.png\"}}"
	exportedhdrs="$exportedhdrs,\"$GF_DOMAIN\":{}"
	locationrules="$locationrules,\"$GF_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"grafana\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"grafana\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS256\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_GRAFANA_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"grafana\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$GF_DOMAIN/login/generic_oauth\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"grafana\":{}"
	post="$post,\"$GF_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$GF_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_KIBANA" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Kibana\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$KB_DOMAIN\",\"description\":\"$KB_DESC\",\"logo\":\"kibana.png\"}}"
	exportedhdrs="$exportedhdrs,\"$KB_DOMAIN\":{}"
	locationrules="$locationrules,\"$KB_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"kibana\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"kibana\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":0,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS256\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_KIBANA_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"kibana\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$KB_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"kibana\":{}"
	post="$post,\"$KB_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$KB_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_PROMETHEUS" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Prometheus\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$PM_DOMAIN\",\"description\":\"$PM_DESC\",\"logo\":\"prometheus.png\"}}"
	exportedhdrs="$exportedhdrs,\"$PM_DOMAIN\":{}"
	locationrules="$locationrules,\"$PM_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"prometheus\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"prometheus\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_PROMETHEUS_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"prometheus\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$PM_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"prometheus\":{}"
	post="$post,\"$PM_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$PM_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MUNIN" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Munin\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MN_DOMAIN\",\"description\":\"$MN_DESC\",\"logo\":\"munin.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MN_DOMAIN\":{}"
	locationrules="$locationrules,\"$MN_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"munin\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"munin\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_MUNIN_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"munin\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$MN_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"munin\":{}"
	post="$post,\"$MN_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MN_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_THANOS" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Thanos\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$TN_DOMAIN\",\"description\":\"Query\",\"logo\":\"thanos.png\"}}"
	exportedhdrs="$exportedhdrs,\"$TN_DOMAIN\":{}"
	locationrules="$locationrules,\"$TN_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"thanos\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"thanos\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_THANOS_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"thanos\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$TN_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"thanos\":{}"
	post="$post,\"$TN_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$TN_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_THANOS_BUCKET" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Thanos\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$TNB_DOMAIN\",\"description\":\"Bucket Web\",\"logo\":\"thanos.png\"}}"
	exportedhdrs="$exportedhdrs,\"$TNB_DOMAIN\":{}"
	locationrules="$locationrules,\"$TNB_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"thanosbucket\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"thanosbucket\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_THANOSBUCKET_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"thanosbucket\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$TNB_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"thanosbucket\":{}"
	post="$post,\"$TNB_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$TNB_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_THANOS_RECEIVE" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Thanos\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$TNR_DOMAIN\",\"description\":\"Receive\",\"logo\":\"thanos.png\"}}"
	exportedhdrs="$exportedhdrs,\"$TNR_DOMAIN\":{}"
	locationrules="$locationrules,\"$TNR_DOMAIN\":{\"default\":\"\$hGroups->{'SSOUsers'}\"}"
	oidcrpattrs="$oidcrpattrs,\"thanosreceive\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"thanosreceive\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_THANOSRECEIVE_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"thanosreceive\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$TNR_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"thanosreceive\":{}"
	post="$post,\"$TNR_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$TNR_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    applist="${applist}},\"$(renderItemId $itemid)-cat\":{\"catname\":\"$PORTAL_CAT_ADMIN\",\"type\":\"category\""
    itemid=$(expr $itemid + 1)
    if test "$DO_ADMIN" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$ADM_DOMAIN\",\"description\":\"$WA_DESC\",\"name\":\"$LLNG_APPNAME Admin\",\"logo\":\"wsweet.png\",\"display\":\"auto\"}}"
	exportedhdrs="$exportedhdrs,\"$ADM_DOMAIN\":{},\"blue-$ADM_DOMAIN\":{},\"green-$ADM_DOMAIN\":{}"
	locationrules="$locationrules,\"$ADM_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"default\":\"accept\"},\"blue-$ADM_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"default\":\"accept\"},\"green-$ADM_DOMAIN\":{\"^/public\":\"skip\",\"^/ping\":\"skip\",\"default\":\"accept\"}"
	post="$post,\"$ADM_DOMAIN\":{},\"blue-$ADM_DOMAIN\":{},\"green-$ADM_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$ADM_DOMAIN\":{},\"blue-$ADM_DOMAIN\":{},\"green-$ADM_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_FUSION" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$FD_DOMAIN\",\"description\":\"$FD_DESC\",\"logo\":\"fusiondirectory.jpg\",\"name\":\"Fusion Directory\",\"display\":\"auto\"}}"
	exportedhdrs="$exportedhdrs,\"$FD_DOMAIN\":{}"
	locationrules="$locationrules,\"$FD_DOMAIN\":{\"default\":\"\$hGroups->{'Admins'}\"}"
	oidcrpattrs="$oidcrpattrs,\"fusion\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"fusion\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_FUSION_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"fusion\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$FD_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"fusion\":{}"
	post="$post,\"$FD_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$FD_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_MAILDROP" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"Maildrop\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$MDS_DOMAIN\",\"description\":\"$MDS_DESC\",\"logo\":\"haraka.png\"}}"
	exportedhdrs="$exportedhdrs,\"$MDS_DOMAIN\":{}"
	locationrules="$locationrules,\"$MDS_DOMAIN\":{\"default\":\"\$hGroups->{'Admins'}\"}"
	post="$post,\"$MDS_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$MDS_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"display\":\"auto\",\"name\":\"Notifications explorer\",\"logo\":\"database.png\",\"description\":\"$SN_DESC\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://manager.$OPENLDAP_ROOT_DOMAIN/notifications.html\"}}"
    itemid=$(expr $itemid + 1)
    if test "$DO_PHPLDAPADMIN" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"PHP LDAP Admin\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$PL_DOMAIN\",\"description\":\"$PL_DESC\",\"logo\":\"phpldapadmin.png\"}}"
	exportedhdrs="$exportedhdrs,\"$PL_DOMAIN\":{}"
	locationrules="$locationrules,\"$PL_DOMAIN\":{\"default\":\"\$hGroups->{'Admins'}\"}"
	post="$post,\"$PL_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$PL_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_RSPAMD" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$RS_DOMAIN\",\"description\":\"$RS_DESC\",\"logo\":\"rspamd.png\",\"name\":\"RSpamd\",\"display\":\"auto\"}}"
	exportedhdrs="$exportedhdrs,\"$RS_DOMAIN\":{}"
	locationrules="$locationrules,\"$RS_DOMAIN\":{\"default\":\"\$hGroups->{'Admins'}\"}"
	post="$post,\"$RS_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$RS_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    if test "$DO_SERVICEDESK" = true; then
	applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"name\":\"ServiceDesk\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://$SD_DOMAIN\",\"description\":\"$SD_DESC\",\"logo\":\"ltb.png\"}}"
	exportedhdrs="$exportedhdrs,\"$SD_DOMAIN\":{}"
	locationrules="$locationrules,\"$SD_DOMAIN\":{\"^/js\":\"skip\",\"^/css\":\"skip\",\"^/images\":\"skip\",\"default\":\"\$hGroups->{'Admins'}\"}"
	oidcrpattrs="$oidcrpattrs,\"servicedesk\":{\"name\":\"uid\",\"email\":\"mail\",\"family_name\":\"uid\",\"fullname\":\"uid\",\"username\":\"uid\"}"
	oidcrpmeta="$oidcrpmeta,\"servicedesk\":{\"oidcRPMetaDataOptionsLogoutSessionRequired\":0,\"oidcRPMetaDataOptionsBypassConsent\":1,\"oidcRPMetaDataOptionsPublic\":0,\"oidcRPMetaDataOptionsLogoutType\":\"front\",\"oidcRPMetaDataOptionsIDTokenSignAlg\":\"RS512\",\"oidcRPMetaDataOptionsIDTokenExpiration\":3600,\"oidcRPMetaDataOptionsAccessTokenExpiration\":3600,\"oidcRPMetaDataOptionsClientSecret\":\"$OPENLDAP_SERVICEDESK_OAUTH2_SECRET\",\"oidcRPMetaDataOptionsClientID\":\"servicedesk\",\"oidcRPMetaDataOptionsRedirectUris\":\"$OPENLDAP_LEMONLDAP_PROTO://$SD_DOMAIN/oauth2/callback\",\"oidcRPMetaDataOptionsRequirePKCE\":0}"
	oidcrpxtrattrs="$oidcrpxtrattrs,\"servicedesk\":{}"
	post="$post,\"$SD_DOMAIN\":{}"
	vhostopts="$vhostopts,\"$SD_DOMAIN\":{}"
	itemid=$(expr $itemid + 1)
    fi
    applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://manager.$OPENLDAP_ROOT_DOMAIN/sessions.html\",\"description\":\"$SS_DESC\",\"name\":\"Sessions explorer\",\"logo\":\"database.png\",\"display\":\"auto\"}}"
    itemid=$(expr $itemid + 1)
    applist="$applist,\"$(renderItemId $itemid)-app\":{\"type\":\"application\",\"options\":{\"logo\":\"configure.png\",\"name\":\"SSO Manager\",\"display\":\"auto\",\"uri\":\"$OPENLDAP_LEMONLDAP_PROTO://manager.$OPENLDAP_ROOT_DOMAIN/manager.html\",\"description\":\"$SC_DESC\"}}"
    itemid=$(expr $itemid + 1)
    applist="${applist}}}"
    casapphdrs="$(echo $casapphdrs | sed 's|{,|{|')}"
    casappmeta="$(echo $casappmeta | sed 's|{,|{|')}"
    exportedhdrs="${exportedhdrs}}"
    locationrules="${locationrules}}"
    oidcrpattrs="$(echo $oidcrpattrs | sed 's|{,|{|')}"
    oidcrpmeta="$(echo $oidcrpmeta | sed 's|{,|{|')}"
    oidcrpxtrattrs="$(echo $oidcrpxtrattrs | sed 's|{,|{|')}"
    post="${post}}"
    samlattrs="$(echo $samlattrs | sed 's|{,|{|')}"
    samlmeta="$(echo $samlmeta | sed 's|{,|{|')}"
    samlxml="$(echo $samlxml | sed 's|{,|{|')}"
    vhostopts="${vhostopts}}"
    echo INFO: Done rendering LLNG VirtualHosts options
    export applist casapphdrs cassappmeta exportedhdrs locationrules post samlattrs samlmeta samlxml vhostopts
}

updateLmEntry()
{
    local ret

    if ! $DID_CLONE; then
	if cloneLmConf; then
	    export DID_CLONE=true
	elif test "$EXIT_ON_ERROR"; then
	    errExit Failed cloning LemonLDAP configuration
	else
	    return 1
	fi
    fi
    if ! test -s /tmp/cached.ldif; then
	if ! ldapsearch -x -H $LADDR -D "$LBRU" -w "$LBRPW" -b "$LBCFG" \
		"(&(objectClass=applicationProcess)(cn=lmConf-$NEXT_ID))" \
		>/tmp/cached.ldif; then
	    echo WARNING: failed caching LLNG configuration >&2
	    rm -f /tmp/cached.ldif
	    return 1
	fi
    fi
    cat <<EOF >/tmp/change.ldif
dn: cn=lmConf-$NEXT_ID,$LBCFG
changetype: modify
delete: description
EOF
    if grep -A1 "^description: {$1}" /tmp/cached.ldif | tail -1 | \
	    grep -E '^(description:|$)' >/dev/null; then
	grep "^description: {$1}" /tmp/cached.ldif >>/tmp/change.ldif
    elif grep "^description: {$1}" /tmp/cached.ldif >/dev/null; then
	cat /tmp/cached.ldif | awk "
	    BEG {
		found = 0;
	    } {
		if (\$1 ~ /^description:/ && \$2 ~ /^{$1}/) {
		    found = 1;
		} else {
		    if (\$1 ~ /^description:/) { found = 0; }
		    if (\$0 == \"\") { found = 0; }
		}
		if (found == 1) { print \$0; }
	    }" >/tmp/lookup.tmp
	WAS=`sed 's|^ ||' /tmp/lookup.tmp | tr -d '\n'`
	cat <<EOF >>/tmp/change.ldif
$WAS
EOF
	rm -f /tmp/lookup.tmp
    else
	MATCH=`/bin/echo -n "{$1}" | base64 | sed 's|......$||'`
	cat /tmp/cached.ldif | awk "
	    BEG {
		found = 0;
	    } {
		if (\$1 ~ /^description::/ && \$2 ~ /^$MATCH/) {
		    found = 1;
		} else {
		    if (\$1 ~ /^description:/) { found = 0; }
		    if (\$0 == \"\") { found = 0; }
		}
		if (found == 1) { print \$0; }
	    }" >/tmp/lookup.tmp
	#WAS=`cat /tmp/lookup.tmp | tr -d '\n' | sed -e 's|description:: ||' -e 's| ||g' | base64 --decode`
	WAS=`sed 's|^ ||' /tmp/lookup.tmp | tr -d '\n'`
	if test "$WAS"; then
	    cat <<EOF >>/tmp/change.ldif
$WAS
EOF
#description: $WAS
	else
	    rm -f /tmp/change.ldif
	fi
	rm -f /tmp/lookup.tmp
    fi
    if ! test -s /tmp/change.ldif; then
	if test "$IS_ENCODED"; then
	    cat <<EOF
dn: cn=lmConf-$NEXT_ID,$LBCFG
changetype: modify
add: description
description:: $2
EOF
	else
	    cat <<EOF
dn: cn=lmConf-$NEXT_ID,$LBCFG
changetype: modify
add: description
description: {$1}$2
EOF
	fi >/tmp/change.ldif
    else
	if grep "^description: {$1}${2}" /tmp/change.ldif \
		>/dev/null 2>/dev/null; then
	    echo "INFO: discarding $1 modification - current value consistent with target"
	    rm -f /tmp/change.ldif
	    return 0
	elif test "$DO_NOT_UPDATE"; then
	    echo "INFO: discarding $1 modification - already set"
	    rm -f /tmp/change.ldif
	    return 0
	fi
	if test "$IS_ENCODED"; then
	    cat <<EOF
-
add: description
description:: $2
EOF
	else
	    cat <<EOF
-
add: description
description: {$1}$2
EOF
	fi >>/tmp/change.ldif
    fi
    cat /tmp/change.ldif | ldapmodify -x -H $LADDR -D "$LBWU" -w "$LBWPW"
    ret=$?
    if test $ret -ne 0; then
	echo Failed applying
	cat /tmp/change.ldif
    fi
    rm -f /tmp/change.ldif
    if test $ret -ne 0; then
	if test "$EXIT_ON_ERROR"; then
	    errExit Failed editing "$1"
	fi
	echo "WARNING: failed editing lmConf-$NEXT_ID::$1" >&2
	return 1
    fi
    echo "INFO: changed $1"
    return 0
}

ensureCertsDefined()
{
    if test -s /certs/oidc.crt -a -s /certs/oidc.key; then
	cat /certs/oidc.crt >/tmp/saml.crt
	cat /certs/oidc.key >/tmp/saml.key
    else
	openssl genrsa -out /tmp/saml.key 4096
	openssl rsa -pubout -in /tmp/saml.key -out /tmp/saml.crt
    fi
    eval `/usr/local/bin/getrsa`
    if test "$RSA_PRIVATE"; then
	OPENLDAP_LEMON_OIDC_SIG_PRIVATE_KEY="${RSA_PRIVATE}"
    fi
    if test "$RSA_PUBLIC"; then
	OPENLDAP_LEMON_OIDC_SIG_PUBLIC_KEY="${RSA_PUBLIC}"
    fi
    unset RSA_PUBLIC RSA_PRIVATE
    rm -f /tmp/saml.crt /tmp/saml.key

    if test -s /certs/saml-sign.crt -a -s /certs/saml-sign.key; then
	cat /certs/saml-sign.crt >/tmp/saml.crt
	cat /certs/saml-sign.key >/tmp/saml.key
    elif test -s /certs/saml.crt -a -s /certs/saml.key; then
	cat /certs/saml.crt >/tmp/saml.crt
	cat /certs/saml.key >/tmp/saml.key
    else
	openssl req -nodes -new -x509 -days 3650 \
	    -keyout /tmp/saml.key -out /tmp/saml.crt -subj /CN=lemon
    fi
    eval `/usr/local/bin/getrsa`
    if test "$RSA_PRIVATE"; then
	OPENLDAP_LEMON_SAML_SIG_PRIVATE_KEY="${RSA_PRIVATE}"
    fi
    if test "$RSA_PUBLIC"; then
	OPENLDAP_LEMON_SAML_SIG_PUBLIC_KEY="${RSA_PUBLIC}"
    fi
    unset RSA_PUBLIC RSA_PRIVATE

    if test -s /certs/saml-enc.crt -a -s /certs/saml-enc.key; then
	cat /certs/saml-enc.crt >/tmp/saml.crt
	cat /certs/saml-enc.key >/tmp/saml.key
    elif ! test -s /certs/saml.crt -a -s /certs/saml.key; then
	rm -f /tmp/saml.crt /tmp/saml.key
	openssl req -nodes -new -x509 -days 3650 \
	    -keyout /tmp/saml.key -out /tmp/saml.crt -subj /CN=lemon
    fi
    eval `/usr/local/bin/getrsa`
    if test "$RSA_PRIVATE"; then
	OPENLDAP_LEMON_SAML_ENC_PRIVATE_KEY="${RSA_PRIVATE}"
    fi
    if test "$RSA_PUBLIC"; then
	OPENLDAP_LEMON_SAML_ENC_PUBLIC_KEY="${RSA_PUBLIC}"
    fi
    rm -f /tmp/saml.crt /tmp/saml.key

    OIDC_KEY_ID_SIG="${OIDC_KEY_ID_SIG:-secretoidc}"
    OIDC_SIG_PUB=$(/bin/echo -en "{oidcServicePublicKeySig}$OPENLDAP_LEMON_OIDC_SIG_PUBLIC_KEY" | base64 -w 0)
    OIDC_SIG_PRIV=$(/bin/echo -en "{oidcServicePrivateKeySig}$OPENLDAP_LEMON_OIDC_SIG_PRIVATE_KEY" | base64 -w 0)
    SAML_ENC_PUB=$(/bin/echo -en "{samlServicePublicKeyEnc}$OPENLDAP_LEMON_SAML_ENC_PUBLIC_KEY" | base64 -w 0)
    SAML_ENC_PRIV=$(/bin/echo -en "{samlServicePrivateKeyEnc}$OPENLDAP_LEMON_SAML_ENC_PRIVATE_KEY" | base64 -w 0)
    SAML_SIG_PUB=$(/bin/echo -en "{samlServicePublicKeySig}$OPENLDAP_LEMON_SAML_SIG_PUBLIC_KEY" | base64 -w 0)
    SAML_SIG_PRIV=$(/bin/echo -en "{samlServicePrivateKeySig}$OPENLDAP_LEMON_SAML_SIG_PRIVATE_KEY" | base64 -w 0)
    updateLmEntry oidcServiceKeyIdSig "$OIDC_KEY_ID_SIG"
    IS_ENCODED=sure updateLmEntry oidcServicePublicKeySig $OIDC_SIG_PUB
    IS_ENCODED=sure updateLmEntry oidcServicePrivateKeySig $OIDC_SIG_PRIV
    IS_ENCODED=sure updateLmEntry samlServicePublicKeyEnc $SAML_ENC_PUB
    IS_ENCODED=sure updateLmEntry samlServicePrivateKeyEnc $SAML_ENC_PRIV
    IS_ENCODED=sure updateLmEntry samlServicePublicKeySig $SAML_SIG_PUB
    IS_ENCODED=sure updateLmEntry samlServicePrivateKeySig $SAML_SIG_PRIV
    unset RSA_PUBLIC RSA_PRIVATE OIDC_SIG_PUB DO_NOT_UPDATE IS_ENCODED \
	OIDC_SIG_PRIV SAML_ENC_PUB SAML_ENC_PRIV SAML_SIG_PUB SAML_SIG_PRIV
}

renderItemId()
{
    if test "$1" -gt 999; then
	echo $1
    elif test "$1" -gt 99; then
	echo 0$1
    elif test "$1" -gt 9; then
	echo 00$1
    else
	echo 000$1
    fi
}
