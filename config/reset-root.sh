if test -z "$sfx"; then
    . /usr/src/openldap/db-env-init
fi
if test "$OPENLDAP_ROOT_PASSWORD"; then
    should_do=true
    if test -s "$OPENLDAP_CONF_DIR/root.pw" -a \
	    -s "$OPENLDAP_CONF_DIR/wsweet.pw"; then
	if grep "^$OPENLDAP_ROOT_PASSWORD" "$OPENLDAP_CONF_DIR/root.pw"; then
	    if $DO_WSWEET; then
		if test -z "$OPENLDAP_WSWEET_PASSWORD"; then
		    OPENLDAP_WSWEET_PASSWORD="$OPENLDAP_ROOT_PASSWORD"
		fi
		if grep "^$OPENLDAP_WSWEET_PASSWORD" \
			"$OPENLDAP_CONF_DIR/wsweet.pw"; then
		    should_do=false
		fi
	    elif grep "^$OPENLDAP_ROOT_PASSWORD" \
		    "$OPENLDAP_CONF_DIR/wsweet.pw"; then
		should_do=false
	    fi
	fi
    fi
    if $should_do; then
	OPENLDAP_ROOT_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_ROOT_PASSWORD")
	if $DO_WSWEET; then
	    WSWEET_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_WSWEET_PASSWORD")
	    if ! sed -e "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
		    -e "s OPENLDAP_ROOT_PASSWORD $OPENLDAP_ROOT_PASSWORD_HASH g" \
		    -e "s OPENLDAP_WSWEET_PASSWORD $WSWEET_SA_PASSWORD_HASH g" \
		    /usr/src/openldap/reset-root-pw.ldif \
		    | ldapmodify -Y EXTERNAL -H ldapi:/// \
		    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
		echo CRITICAL: failed initializing OpenLDAP root password
		exit 1
	    else
		echo 'NOTICE: (re)initialized OpenLDAP root password'
	    fi
	    echo "$OPENLDAP_ROOT_PASSWORD" >"$OPENLDAP_CONF_DIR/root.pw"
	    echo "$OPENLDAP_WSWEET_PASSWORD" >"$OPENLDAP_CONF_DIR/wsweet.pw"
	else
	    if ! sed -e "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
		    -e "s OPENLDAP_ROOT_PASSWORD $OPENLDAP_ROOT_PASSWORD_HASH g" \
		    -e "s OPENLDAP_WSWEET_PASSWORD $OPENLDAP_ROOT_PASSWORD_HASH g" \
		    /usr/src/openldap/reset-root-pw.ldif \
		    | ldapmodify -Y EXTERNAL -H ldapi:/// \
		    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
		echo CRITICAL: failed initializing OpenLDAP root password
		exit 1
	    else
		echo 'NOTICE: (re)initialized OpenLDAP root password'
	    fi
	    echo "$OPENLDAP_ROOT_PASSWORD" >"$OPENLDAP_CONF_DIR/root.pw"
	    echo "$OPENLDAP_ROOT_PASSWORD" >"$OPENLDAP_CONF_DIR/wsweet.pw"
	fi
    else
	echo INFO: directory root password already set
    fi
else
    echo CRITICAL: Root Password not set - can not initialize
    exit 1
fi
