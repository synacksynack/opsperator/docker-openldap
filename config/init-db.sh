if test -z "$OPENLDAP_CONF_DIR" -o -z "$sfx"; then
    . /usr/src/openldap/db-env-init
fi

ls /usr/src/openldap/root-conf-orig | while read item
    do
	if test -f "$OPENLDAP_CONF_DIR/$item" -o \
		-d "$OPENLDAP_CONF_DIR/$item"; then
	    continue
	fi
	cp -rf "/usr/src/openldap/root-conf-orig/$item" \
	    $OPENLDAP_CONF_DIR/
    done
for d in /var/run/slapd $OPENLDAP_CONF_DIR/slapd.d
do
    test -d "$d" || mkdir -p "$d"
done

if ! test -f "$OPENLDAP_CONF_DIR/CONFIGURED"; then
    OPENLDAP_ROOT_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_ROOT_PASSWORD")
    WSWEET_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_WSWEET_PASSWORD")
    cp /usr/src/openldap/DB_CONFIG /var/lib/ldap/DB_CONFIG
    if test "$OPENLDAP_DB_BACKEND" = mdb; then
	DB_OC=olcMdbConfig
    else
	if test "$OPENLDAP_FLAVOR" = ltb; then
	    echo "WARNING: LTB removed support for bdb/hdb, switching to mdb backend"
	    DB_OC=olcMdbConfig
	    OPENLDAP_DB_BACKEND=mdb
	else
	    DB_OC=olcHdbConfig
	    if ! test "$OPENLDAP_DB_BACKEND" = hdb; then
		echo "WARNING: unsupported $OPENLDAP_DB_BACKEND backend"
		echo "    switching to hdb"
		OPENLDAP_DB_BACKEND=hdb
	    fi
	    DB_OC=olcHdbConfig
	fi
    fi
    rm -rf "$OPENLDAP_CONF_DIR"/slapd.d/cn=config/olcDatabase={[1-9]}mdb.ldif \
	"$OPENLDAP_CONF_DIR"/slapd.d/cn=config/olcDatabase={[1-9]}hdb.ldif \
	"$OPENLDAP_CONF_DIR"/slapd.d/cn=config/olcDatabase={[1-9]}monitor.ldif \
	"$OPENLDAP_CONF_DIR"/slapd.d/cn=config/cn=schema/cn={[1-9]}*.ldif \
	"$OPENLDAP_CONF_DIR"/slapd.d/cn=config/olcDatabase={[1-9]}mdb \
	"$OPENLDAP_CONF_DIR"/slapd.d/cn=config/olcDatabase={[1-9]}hdb
    if test -s /usr/lib64/openldap/check_password.so \
	    -o -s /usr/local/openldap/lib64/check_password.so \
	    -o -s /usr/lib/ldap/pqchecker.so; then
	sed -i 's|.*useCracklib .*|useCracklib 0|' \
	    "$OPENLDAP_CONF_DIR/check_password.conf"
    fi
    /usr/local/bin/hack-slapd-ldapi
    if ! start_with_ldapi; then
	echo CRITICAL - could not start slapd
	exit 1
    fi
    for ldif in "$OPENLDAP_CONF_DIR/schema/cosine" \
	"$OPENLDAP_CONF_DIR/schema/inetorgperson" \
	"$OPENLDAP_CONF_DIR/schema/ppolicy" \
	"$OPENLDAP_CONF_DIR/schema/dyngroup" \
	/usr/src/openldap/template-fd \
	/usr/src/openldap/rfc2307bis \
	/usr/src/openldap/mail-fd \
	/usr/src/openldap/mail-fd-conf \
	/usr/src/openldap/ldapns \
	/usr/src/openldap/core-fd \
	/usr/src/openldap/core-fd-conf \
	/usr/src/openldap/openssh-lpk \
	/usr/src/openldap/wsweet
    do
	if test -s "$ldif.ldif"; then
	    bn=$(basename "$ldif.ldif")
	    if ls "$OPENLDAP_CONF_DIR"/slapd.d/cn=config/cn=schema/*}$bn \
		    >/dev/null 2>&1; then
		echo "NOTICE: schema $bn already loaded"
	    elif ! cat "$ldif.ldif" | ldapadd -Y EXTERNAL -H ldapi:/// \
		    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
		echo "CRITICAL: Failed loading schema $bn"
		exit 1
	    fi
	fi
    done
    if test "$OPENLDAP_FLAVOR" = ltb; then
	modsfx=-ltb
    else
	modsfx=$sfx
    fi
    if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
	    -e "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
	    -e "s/OPENLDAP_ROOT_DN/$OPENLDAP_ROOT_DN_PREFIX/g" \
	    -e "s OPENLDAP_ROOT_PASSWORD $OPENLDAP_ROOT_PASSWORD_HASH g" \
	    -e "s OPENLDAP_PORT $OPENLDAP_BIND_LDAP_PORT g" \
	    "/usr/src/openldap/load_modules$modsfx.ldif" | \
	    ldapmodify -Y EXTERNAL -H ldapi:/// \
	    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
	echo CRITICAL: Failed loading modules
	exit 1
    fi
    if ! cat /usr/src/openldap/single-mail.ldif | \
	    ldapmodify -Y EXTERNAL -H ldapi:/// \
	    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
	echo CRITICAL: Failed patching OpenLDAP Core Schema
	exit 1
    fi
    uidNumber=`id -u`
    gidNumber=`id -g`
    if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
	    -e "s UIDNUMBER $uidNumber g" \
	    -e "s GIDNUMBER $gidNumber g" \
	    "/usr/src/openldap/create_monitordb.ldif" | \
	    ldapadd -Y EXTERNAL -H ldapi:/// \
	    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
	echo CRITICAL: Failed creating monitor database
	exit 1
    fi
    if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
	    -e "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
	    -e "s DBOC $DB_OC g" \
	    -e "s/OPENLDAP_ROOT_DN/$OPENLDAP_ROOT_DN_PREFIX/g" \
	    -e "s UIDNUMBER $uidNumber g" \
	    -e "s OPENLDAP_ROOT_PASSWORD $OPENLDAP_ROOT_PASSWORD_HASH g" \
	    "/usr/src/openldap/create_db.ldif" | \
	    ldapadd -Y EXTERNAL -H ldapi:/// \
	    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
	echo CRITICAL: Failed creating main database
	exit 1
    fi
    . /usr/src/openldap/reset-root.sh
    if ! sed -e "s OPENLDAP_ROOT_PASSWORD $OPENLDAP_ROOT_PASSWORD_HASH g" \
	    -e "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
	    -e "s/OPENLDAP_ROOT_DN/$OPENLDAP_ROOT_DN_PREFIX/g" \
	    -e "s OPENLDAP_WSWEET_PASSWORD $WSWEET_SA_PASSWORD_HASH g" \
	    -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
	    /usr/src/openldap/first_config$modsfx.ldif | \
	    ldapmodify -Y EXTERNAL -H ldapi:/// \
	    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
	echo CRITICAL: Failed initializing databases configuration
	exit 1
    fi
    #for ldif in memberof refint ppolicy unique dynlist syncrepl indexes gosa
    for ldif in memberof refint ppolicy unique dynlist syncrepl indexes \
	active_sort
    do
	if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
		-e "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
		-e "s OPENLDAP_PORT $OPENLDAP_BIND_LDAP_PORT g" \
		"/usr/src/openldap/configure_$ldif.ldif" | \
		ldapadd -Y EXTERNAL -H ldapi:/// \
		-d $OPENLDAP_INIT_DEBUG_LEVEL; then
	    echo "CRITICAL: Failed configuring module $ldif"
	    exit 1
	fi
    done
    . /usr/src/openldap/reset-tls.sh
    if test "$SERVER_ID"; then
	if ! sed "s OPENLDAP_SERVER_ID $SERVER_ID g" \
		/usr/src/openldap/syncrepl.ldif | \
		ldapmodify -Y EXTERNAL -H ldapi:/// \
		-d $OPENLDAP_INIT_DEBUG_LEVEL; then
	    echo CRITICAL: Failed initializing OpenLDAP SyncRepl Server ID
	    exit 1
	fi
	unset SERVER_ID
    fi
    if ! sed "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
	    /usr/src/openldap/set-hash.ldif | \
	    ldapmodify -Y EXTERNAL -H ldapi:/// \
	    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
	echo CRITICAL: Failed setting OpenLDAP Default Password Hash Algorithm
	exit 1
    fi
    if test -s /provision/init.ldif; then
	echo NOTICE: skiping directory init - found backup to import
	SHOULD_IMPORT_BACKUP=true
    elif $OPENLDAP_FULL_INIT; then
	if test "$OPENLDAP_SERVER_ID" = 0; then
	    check="$OPENLDAP_BASENAME-1.$OPENLDAP_STATEFULSET_NAME"
	    if ldapsearch \
		-H "ldap://$check:$OPENLDAP_BIND_LDAP_PORT/" \
		-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
		-w "$OPENLDAP_ROOT_PASSWORD" -b "$OPENLDAP_ROOT_DN_SUFFIX" \
		"($OPENLDAP_ROOT_DN_PREFIX)" >/dev/null 2>&1; then
		echo "INFO: Found valid $OPENLDAP_BASENAME-1"
		echo "INFO: Skipping full init, bootstrap replica"
		OPENLDAP_FULL_INIT=false
	    fi
	    unset check
	fi
	if $OPENLDAP_FULL_INIT; then
	    . /usr/src/openldap/init-directory.sh
	    CPPATH=
	    if test -s /usr/lib64/openldap/check_password.so; then
		CPPATH=/usr/lib64/openldap
	    elif test -s /usr/local/openldap/lib64/check_password.so; then
		CPPATH=/usr/local/openldap/lib64
	    fi
	    if test "$CPPATH"; then
		if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
			-e "s CPPATH $CPPATH g" \
			/usr/src/openldap/configure_checkpassword.ldif \
			| ldapmodify -x \
			-H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
			-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
			-w "$OPENLDAP_ROOT_PASSWORD" \
			-d $OPENLDAP_INIT_DEBUG_LEVEL; then
		    echo CRITICAL: Failed configuring OpenLDAP Custom Password Checker
		    exit 1
		fi
	    elif test -s /usr/lib/ldap/pqchecker.so; then
		if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
			/usr/src/openldap/configure_pqchecker.ldif \
			| ldapmodify -x \
			-H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
			-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
			-w "$OPENLDAP_ROOT_PASSWORD" \
			-d $OPENLDAP_INIT_DEBUG_LEVEL; then
		    echo CRITICAL: Failed configuring OpenLDAP Custom Password Checker
		    exit 1
		fi
	    fi
	fi
    fi

    stop_ldapi
    LOG=`slaptest 2>&1`
    CHECKSUM_ERR=$(echo "$LOG" | grep -Po "(?<=ldif_read_file: checksum error on \").+(?=\")")
    for err in $CHECKSUM_ERR
    do
	cat <<EOF
=== WARNING ===
The file $err has a checksum error.
Ensure that this file was not edited manually, or re-calculate its checksum.
EOF
    done >&2
    date +%s >"$OPENLDAP_CONF_DIR/CONFIGURED"
    unset CHECKSUM_ERR DC_PREFIX OPENLDAP_ORG_SHORT DB_OC \
	OPENLDAP_ROOT_PASSWORD_HASH WSWEET_SA_PASSWORD_HASH
else
    ls -l "$OPENLDAP_CONF_DIR/CONFIGURED"
    cat "$OPENLDAP_CONF_DIR/CONFIGURED"
fi
