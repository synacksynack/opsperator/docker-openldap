if test -z "$OPENLDAP_CONF_DIR" -o -z "$sfx"; then
    . /usr/src/openldap/db-env-init
fi

if test \( "$OPENLDAP_STATIC_NEIGHBORS" -a "$OPENLDAP_MY_PUBLIC_ID" \) \
	-o "$OPENLDAP_BASENAME"; then
    MISSING=`list_missing_replicates`
    if test "$MISSING"; then
	if ! start_with_ldapi; then
	    echo CRITICAL - could not start slapd
	    exit 1
	fi
	if ! ls "$OPENLDAP_CONF_DIR/slapd.d/cn=config/olcDatabase={2}$OPENLDAP_DB_BACKEND/" \
		2>/dev/null | grep syncprov >/dev/null; then
	    if ! sed "s|DBBACKEND|$OPENLDAP_DB_BACKEND|" \
		    /usr/src/openldap/configure_syncrepl.ldif \
		    | ldapadd -Y EXTERNAL -H ldapi:/// \
		    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
		echo WARNING: Failed loading OpenLDAP SyncRepl Overlay
	    fi
	    if test "$OPENLDAP_STATIC_NEIGHBORS" -a \
		    "$OPENLDAP_MY_PUBLIC_ID"; then
		SERVER_ID=`echo "$OPENLDAP_STATIC_NEIGHBORS" | tr ' ' '\n' | grep -n "^$OPENLDAP_MY_PUBLIC_ID" | awk -F: '{print $1}'`
	    else
		SERVER_ID=`expr $OPENLDAP_SERVER_ID + 1`
	    fi
	    if ! sed "s OPENLDAP_SERVER_ID $SERVER_ID g" \
		    /usr/src/openldap/syncrepl.ldif \
		    | ldapmodify -Y EXTERNAL -H ldapi:/// \
		    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
		echo WARNING: Failed setting OpenLDAP Server ID
	    fi
	fi
	_msg=" not yet configured as syncrepl remote, adding it now"
	REPLPORT=$OPENLDAP_BIND_LDAP_PORT
	REPLPROTO=ldap
	if test "$TLS_EVERYWHERE" = true; then
	    REPLPORT=$OPENLDAP_BIND_LDAPS_PORT
	    REPLPROTO=ldaps
	fi
	for add in $MISSING
	do
	    if test "$OPENLDAP_STATIC_NEIGHBORS" -a \
		    "$OPENLDAP_MY_PUBLIC_ID"; then
		echo "NOTICE: $add$_msg"
		SERVER_ID=`echo "$OPENLDAP_STATIC_NEIGHBORS" | tr ' ' '\n' | grep -n "^$add" | awk -F: '{print $1}'`
		if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
			-e "s BASENAME-RID.CLUSTER_DOMAIN $add g" \
			-e "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
			-e "s LDAP_PORT $REPLPORT g" \
			-e "s LDAP_PROTO $REPLPROTO g" \
			-e "s NID $SERVER_ID g" \
			-e "s|OPENLDAP_SYNCREPL_PASSWORD|$OPENLDAP_SYNCREPL_PASSWORD|g" \
			/usr/src/openldap/syncrepl_repl.ldif \
			| ldapadd -Y EXTERNAL -H ldapi:/// \
			-d $OPENLDAP_INIT_DEBUG_LEVEL; then
		    echo WARNING: failed initializing replication with $add
		else
		    echo $add:`date +%s` >>"$OPENLDAP_CONF_DIR/.repl-configured"
		fi
	    else
		echo "NOTICE: $OPENLDAP_BASENAME-$add$_msg"
		SERVER_ID=`expr $add + 1`
		if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
			-e "s BASENAME $OPENLDAP_BASENAME g" \
			-e "s CLUSTER_DOMAIN $OPENLDAP_STATEFULSET_NAME g" \
			-e "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
			-e "s LDAP_PORT $REPLPORT g" \
			-e "s LDAP_PROTO $REPLPROTO g" \
			-e "s NID $SERVER_ID g" \
			-e "s|OPENLDAP_SYNCREPL_PASSWORD|$OPENLDAP_SYNCREPL_PASSWORD|g" \
			-e "s RID $add g" \
			/usr/src/openldap/syncrepl_repl.ldif \
			| ldapadd -Y EXTERNAL -H ldapi:/// \
			-d $OPENLDAP_INIT_DEBUG_LEVEL; then
		    echo WARNING: failed initializing replication with $OPENLDAP_BASENAME-$add
		else
		    echo $OPENLDAP_BASENAME-$add:`date +%s` >>"$OPENLDAP_CONF_DIR/.repl-configured"
		fi
	    fi
	done
	if test -s "$OPENLDAP_CONF_DIR/.repl-configured" \
		-a \! -s "$OPENLDAP_CONF_DIR/.repl-mirror"; then
	    if ! sed "s DBBACKEND $OPENLDAP_DB_BACKEND g" \
		    /usr/src/openldap/syncrepl_mirror.ldif \
		    | ldapmodify -Y EXTERNAL -H ldapi:/// \
		    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
		echo WARNING: Failed enabling OpenLDAP SyncRepl Mirror Mode
	    else
		date >"$OPENLDAP_CONF_DIR/.repl-mirror"
	    fi
	fi
	stop_ldapi
    fi
    echo Running multi-master cluster composed from:
    (
	if test "$OPENLDAP_STATIC_NEIGHBORS" -a "$OPENLDAP_MY_PUBLIC_ID"; then
	    echo $OPENLDAP_MY_PUBLIC_ID
	else
	    echo $OPENLDAP_BASENAME-$OPENLDAP_SERVER_ID
	fi
	cat "$OPENLDAP_CONF_DIR/.repl-configured" 2>/dev/null
    ) | sort | sed 's|^\(.*\)$|  - \1|'
    unset MISSING OPENLDAP_BASENAME SERVER_ID
else
    cat <<EOF >&2
WARNING: running standalone OpenLDAP server is not recommended for production
	 consider setting up some kind of replication. This image offers with
	 OpenShift-compliant multi-master support.
EOF
fi
