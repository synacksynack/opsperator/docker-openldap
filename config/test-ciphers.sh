#!/bin/bash
# source: https://gist.github.com/jahir/23c4202eee12e377ef3acf1dcdc7c776

CIPHERS=ALL:eNULL
DELAY=${2:-0.1}
SERVER=${1:?usage: $0 <host:port> [delay, default is ${DELAY}s] [ciphers, default is ${CIPHERS}]}
MAXLEN=$(openssl ciphers "$CIPHERS" | sed -e 's/:/\n/g' | awk '{if (length > L) {L=length}}END{print L}')

echo Using $(openssl version).

declare -A TLSMAP=( [tls1_1]=cipher [tls1_2]=cipher [tls1_3]=ciphersuites )

for tlsver in "${!TLSMAP[@]}"
do
    echo "Using $tlsver"
    ciphers=$(openssl ciphers -$tlsver -s "$CIPHERS" | sed -e 's/:/ /g')
    for cipher in ${ciphers[@]}
    do
	in=$(openssl s_client -$tlsver -${TLSMAP[$tlsver]} "$cipher" -connect $SERVER </dev/null 2>&1)
	if [[ "$in" =~ ":error:" ]] ; then
	    result="NO ($(echo -n $in | cut -d':' -f6))"
	elif [[ "$in" =~ "Cipher is ${cipher}" || "$in" =~ "Cipher    :" ]] ; then
	    result='YES'
	else
	    result="UNKNOWN RESPONSE\n$in"
	fi
	printf 'Testing %-*s ... %s\n' "$MAXLEN" "$cipher" "$result"
	sleep $DELAY
    done
done
