#!/bin/sh

if test -z "$sfx"; then
    . /usr/src/openldap/db-env-init
fi
should_rehash=false
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	if test -d /etc/pki/ca-trust/source/anchors; then
	    dir=/etc/pki/ca-trust/source/anchors
	else
	    dir=/usr/local/share/ca-certificates
	fi
	d=`echo $f | sed 's|/|-|g'`
	if ! test -s $dir/kube$d; then
	    if ! cat $f >$dir/kube$d; then
		echo WARNING: failed installing $f certificate authority >&2
	    else
		should_rehash=true
	    fi
	fi
    fi
done
if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash

RESET_SSL=${RESET_SSL:-false}
if test -s "$OPENLDAP_CONF_DIR/tls/server.crt" -a \
	-s "$OPENLDAP_CONF_DIR/tls/server.key" -a \
	"$RESET_SSL" = false; then
    echo Skipping OpenLDAP SSL configuration - already initialized
elif test -s /certs/tls.crt -a -s /certs/tls.key; then
    echo Initializing OpenLDAP SSL configuration
    test -d "$OPENLDAP_CONF_DIR/tls" || mkdir "$OPENLDAP_CONF_DIR/tls"
    cat /certs/tls.crt >"$OPENLDAP_CONF_DIR/tls/server.crt"
    cat /certs/tls.key >"$OPENLDAP_CONF_DIR/tls/server.key"
    chmod 0640 "$OPENLDAP_CONF_DIR/tls/server.key"
    if test "$OPENLDAP_FLAVOR" = ltb; then
	msfx=-ltb
    else
	msfx=$sfx
    fi
    if ! cat /usr/src/openldap/configure_tls$msfx.ldif \
	    | ldapadd -Y EXTERNAL -H ldapi:/// \
	    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
	echo CRITICAL: Failed installing LDAP Certificates
	exit 1
    fi
    if test -z "$CAFILE"; then
	cat <<EOT >&2
WARNING: Looks like there is no CA chain defined!
	 assuming it is not required or otherwise included in server
	 certificate definition
EOT
    else
	cat "$CAFILE" >"$OPENLDAP_CONF_DIR/tls/ca.crt"
	if ! cat /usr/src/openldap/configure_ca$sfx.ldif \
		| ldapadd -Y EXTERNAL -H ldapi:/// \
		-d $OPENLDAP_INIT_DEBUG_LEVEL; then
	    echo CRITICAL: Failed installing LDAP CA Chain
	    exit 1
	fi
    fi
    if ! test -s /certs/dhparam.pem; then
	if ! test -s /etc/ssl/dhparam.pem; then
	    if ! test -s /etc/pki/dhparam.pem; then
		echo No DH found alongside server certificate key pair, generating one
		echo This may take some time...
		if ! openssl dhparam -out "$OPENLDAP_CONF_DIR/tls/dhparam.pem" \
			${DUMMY_DH_SIZE:-2048}; then
		    echo WARNING: Failed generating DH params
		    rm -f "$OPENLDAP_CONF_DIR/tls/dhparam.pem"
		fi
	    else
		cat /etc/pki/dhparam.pem \
		    >"$OPENLDAP_CONF_DIR/tls/dhparam.pem"
	    fi
	else
	    cat /etc/ssl/dhparam.pem >"$OPENLDAP_CONF_DIR/tls/dhparam.pem"
	fi
    else
	cat /certs/dhparam.pem >"$OPENLDAP_CONF_DIR/tls/dhparam.pem"
    fi
    if test -s "$OPENLDAP_CONF_DIR/tls/dhparam.pem"; then
	if ! cat /usr/src/openldap/configure_dh$sfx.ldif \
		| ldapadd -Y EXTERNAL -H ldapi:/// \
		-d $OPENLDAP_INIT_DEBUG_LEVEL; then
	    echo CRITICAL: Failed installing LDAP DH Params
	    exit 1
	fi
    fi
fi
