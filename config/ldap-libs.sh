start_with_ldapi()
{
    slapd -h "ldap://:$OPENLDAP_BIND_LDAP_PORT/ ldaps://:$OPENLDAP_BIND_LDAPS_PORT/ ldapi:///" \
	-d $OPENLDAP_INIT_DEBUG_LEVEL &
    cpt=0
    while test "$cpt" -lt 30
    do
	if bash -c \
		"echo dummy >/dev/tcp/127.0.0.1/$OPENLDAP_BIND_LDAP_PORT" \
		>/dev/null 2>&1; then
	    break
	fi
	cpt=`expr $cpt + 1`
	sleep 1
    done
    if test "$cpt" -eq 30; then
	echo CRITICAL: slapd did not start correctly
	exit 1
    fi
}

stop_ldapi()
{
    cpt=0
    pid=$(ps -ax | awk '/sla[p]d .*-h/{print $1}')
    if test "$pid"; then
	kill -2 $pid || echo $?
	while test "$cpt" -lt 30
	do
	    if ! ps -A 2>/dev/null | grep -E "^[ ]*$pid[ ]"; then
		break
	    fi
	    echo WARNING: slapd still running
	    cpt=`expr $cpt + 1`
	    sleep 1
	done
	if test "$cpt" -eq 30; then
	    echo CRITICAL: slapd did not stop correctly
	    exit 1
	fi
    else
	echo WARNING: slapd not found
    fi
}

list_elligible_patches()
{
    eval `echo $VERSION | sed 's|^\([0-9]*\)\.\([0-9]*\)\.\([0-9]*\)$|left1=\1 left2=\2 left3=\3|'`
    ls /usr/src/openldap/config-updates | sort -V | while read UPGRADE
	do
	    eval `echo $UPGRADE | sed 's|^\([0-9]*\)\.\([0-9]*\)\.\([0-9]*\)$|right1=\1 right2=\2 right3=\3|'`
	    if test "$left1" -gt "$right1"; then
		continue
	    elif test "$left2" -gt "$right2"; then
		continue
	    elif test "$left3" -ge "$right3"; then
		continue
	    fi
	    echo $UPGRADE
	done
}

list_missing_replicates()
{
    local check

    if test "$OPENLDAP_STATIC_NEIGHBORS" -a "$OPENLDAP_MY_PUBLIC_ID"; then
	for member in $OPENLDAP_STATIC_NEIGHBORS
	do
	    if echo "$member" | grep : >/dev/null; then
		#FIXME: cross-SDN replication should involve TLS
		# though initial replication implementation sets LDAP_PROTO to ldap
		# would need to patch further, ... focusing on my demo right now
		check=`echo $member | sed 's|:|/|'`
	    else
		check="$member/$OPENLDAP_BIND_LDAP_PORT"
	    fi
	    if test "$member" = "$OPENLDAP_MY_PUBLIC_ID"; then
		echo NOTICE: do not configure replication against local / $member
		continue
	    elif grep "^$member:" "$OPENLDAP_CONF_DIR/.repl-configured" \
		    >/dev/null 2>&1; then
		echo NOTICE: do not reconfigure replication against remote / $member
		continue
	    elif bash -c "echo dummy >/dev/tcp/$check" >/dev/null 2>&1; then
		echo NOTICE: $member is alive
	    else
		echo NOTICE: $member unreachable, assuming remote does not exist
	    fi >&2
	    echo $member
	done
    else
	check=0
	while :
	do
	    who=$OPENLDAP_BASENAME-$check.$OPENLDAP_STATEFULSET_NAME/$OPENLDAP_BIND_LDAP_PORT
	    if test "$check" = "$OPENLDAP_SERVER_ID"; then
		echo NOTICE: do not configure replication against local
		check=`expr $check + 1`
		continue
	    elif grep "^$OPENLDAP_BASENAME-$check:" \
		    "$OPENLDAP_CONF_DIR/.repl-configured" >/dev/null 2>&1; then
		echo NOTICE: do not reconfigure replication against remote / $who
		check=`expr $check + 1`
		continue
	    elif bash -c "echo dummy >/dev/tcp/$who" >/dev/null 2>&1; then
		echo NOTICE: $OPENLDAP_BASENAME-$check is alive
	    else
		echo NOTICE: $who unreachable, assuming remote does not exist
		if ! test -s "$OPENLDAP_CONF_DIR/.repl-configured"; then
		    cat <<EOF
WARNING: when first deploying a statefulset, once done starting all members of
	 your cluster, make sure to sequentially reboot all your containers but
	 the last one, as we've only configured replication against remotes that
	 were reachable during service initialization
EOF
		fi
		break
	    fi >&2
	    echo $check
	    check=`expr $check + 1`
	done
    fi
}

markConfigured()
{
    cat <<EOF >$MARK_FILE
HAS_ADMIN=$DO_ADMIN
HAS_AIRSONIC=$DO_AIRSONIC
HAS_ALERTMANAGER=$DO_ALERTMANAGER
HAS_ARA=$DO_ARA
HAS_ARTIFACTORY=$DO_ARTIFACTORY
HAS_AUTHPROXY=$DO_AUTHPROXY
HAS_AWX=$DO_AWX
HAS_BLUEMIND=$DO_BLUEMIND
HAS_BROWSEABLE=true
HAS_CODIMD=$DO_CODIMD
HAS_CYRUS=$DO_CYRUS
HAS_DB_BACKEND=$OPENLDAP_DB_BACKEND
HAS_DIASPORA=$DO_DIASPORA
HAS_DOKUWIKI=$DO_DOKUWIKI
HAS_DRAW=$DO_DRAW
HAS_ERRBIT=$DO_ERRBIT
HAS_ESCOMRADE=$DO_ESCOMRADE
HAS_ETHERPAD=$DO_ETHERPAD
HAS_EXPORTERS=$DO_EXPORTERS
HAS_FUSION=$DO_FUSION
HAS_GERRIT=$DO_GERRIT
HAS_GITEA=$DO_GITEA
HAS_GOGS=$DO_GOGS
HAS_GRAFANA=$DO_GRAFANA
HAS_GREENLIGHT=$DO_GREENLIGHT
HAS_JENKINS=$DO_JENKINS
HAS_KIBANA=$DO_KIBANA
HAS_KIWIIRC=$DO_KIWIIRC
HAS_LEMON=$DO_LEMON
HAS_LEMONSESSION=$DO_LEMONSESSION
HAS_LLNG=$SHIPS_LLNG
HAS_LLNG_APPNAME=$LLNG_APPNAME
HAS_LLNG_BACKGROUND=$LLNG_BACKGROUND
HAS_LLNG_LANG=$LLNG_LANG
HAS_LLNG_SKIN=$LLNG_SKIN
HAS_MAILDROP=$DO_MAILDROP
HAS_MAILHOG=$DO_MAILHOG
HAS_MASTODON=$DO_MASTODON
HAS_MATOMO=$DO_MATOMO
HAS_MATTERMOST=$DO_MATTERMOST
HAS_MEDUSA=$DO_MEDUSA
HAS_MINIO=$DO_MINIO
HAS_MINIOCONSOLE=$DO_MINIOCONSOLE
HAS_MUNIN=$DO_MUNIN
HAS_NEXTCLOUD=$DO_NEXTCLOUD
HAS_NEXUS=$DO_NEXUS
HAS_NEWZNAB=$DO_NEWZNAB
HAS_PEERTUBE=$DO_PEERTUBE
HAS_PHPLDAPADMIN=$DO_PHPLDAPADMIN
HAS_POSTFIX=$DO_POSTFIX
HAS_PROMETHEUS=$DO_PROMETHEUS
HAS_REGISTRYCONSOLE=$DO_REGISTRYCONSOLE
HAS_RELEASEBELL=$DO_RELEASEBELL
HAS_ROCKET=$DO_ROCKET
HAS_ROUNDCUBE=$DO_ROUNDCUBE
HAS_RSPAMD=$DO_RSPAMD
HAS_SABNZBD=$DO_SABNZBD
HAS_SERVICEDESK=$DO_SERVICEDESK
HAS_SESSIONS_BACKEND=$LLNG_SESSIONS_BACKEND
HAS_SMTP_SERVER=$OPENLDAP_SMTP_SERVER
HAS_SMTP_PORT=$OPENLDAP_SMTP_PORT
HAS_SOGO=$DO_SOGO
HAS_SONARQUBE=$DO_SONARQUBE
HAS_SOPLANNING=$DO_SOPLANNING
HAS_SSOAPP=$DO_SSOAPP
HAS_SSP=$DO_SSP
HAS_SYMPA=$DO_SYMPA
HAS_SYSPASS=$DO_SYSPASS
HAS_THANOS=$DO_THANOS
HAS_THANOS_BUCKET=$DO_THANOS_BUCKET
HAS_THANOS_RECEIVE=$DO_THANOS_RECEIVE
HAS_TINYTINYRSS=$DO_TINYTINYRSS
HAS_TRANSMISSION=$DO_TRANSMISSION
HAS_WEKAN=$DO_WEKAN
HAS_WHITEPAGES=$DO_WHITEPAGES
HAS_WORDPRESS=$DO_WORDPRESS
HAS_WSWEET=$DO_WSWEET
HAS_ZPUSH=$DO_ZPUSH
EOF
    echo "INFO: done configuring"
}
