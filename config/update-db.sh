if test -z "$OPENLDAP_CONF_DIR" -o -z "$sfx"; then
    . /usr/src/openldap/db-env-init
fi

APPLY_VERSIONS=`list_elligible_patches`
RESETPW=false
SHOULD_IMPORT_BACKUP=${SHOULD_IMPORT_BACKUP:-false}
if ! grep "^$OPENLDAP_ROOT_PASSWORD$" "$OPENLDAP_CONF_DIR/root.pw"; then
    RESETPW=true
fi >/dev/null 2>&1
if test "$APPLY_VERSIONS" -o $RESETPW = true -o "$RESET_SSL" = true; then
    if grep back_hdb "$OPENLDAP_CONF_DIR/slapd.d/cn=config/cn=module{0}.ldif" \
	    >/dev/null 2>&1; then
	sed -i '/back_hdb/d' \
	    "$OPENLDAP_CONF_DIR/slapd.d/cn=config/cn=module{0}.ldif"
	/usr/local/bin/hack-slapd-ldapi
    fi
    if ! start_with_ldapi; then
	echo CRITICAL - could not start slapd
	exit 1
    fi
    if $RESETPW; then
	. /usr/src/openldap/reset-root.sh
    fi
    if test "$RESET_SSL" = true; then
	. /usr/src/openldap/reset-tls.sh
    fi
    FINE=true
    for TARGET in $APPLY_VERSIONS
    do
	ls /usr/src/openldap/config-updates/$TARGET/cn=config/*.ldif \
	    2>/dev/null | while read ldif
	    do
		if ! sed "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
			$ldif | ldapmodify -Y EXTERNAL -H ldapi:/// \
			-d $OPENLDAP_INIT_DEBUG_LEVEL; then
		    echo WARNING: failed importing $ldif
		    FINE=false
		    break
		fi
	    done
	if $FINE; then
	    ls /usr/src/openldap/config-updates/$TARGET/schemas/*.ldif \
		2>/dev/null | while read ldif
		do
		    if ! sed "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
			    $ldif | ldapadd -Y EXTERNAL -H ldapi:/// \
			    -d $OPENLDAP_INIT_DEBUG_LEVEL; then
			echo WARNING: failed importing $ldif, aborting
			FINE=false
			break
		    fi
		done
	fi
	if $SHOULD_IMPORT_BACKUP; then
	    echo NOTICE: skipping database patches - about to import backup
	elif $OPENLDAP_FULL_INIT; then
	    if $FINE; then
		ls /usr/src/openldap/config-updates/$TARGET/main/*.ldif \
		    2>/dev/null | while read ldif
		    do
			if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
				$ldif | ldapadd -x -H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
				-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
				-w "$OPENLDAP_ROOT_PASSWORD"; then
			    echo WARNING: failed importing $ldif, aborting
			    FINE=false
			    break
			fi
		    done
	    fi
	    if $FINE; then
		ls /usr/src/openldap/config-updates/$TARGET/scripts/*.sh \
		    2>/dev/null | while read fix
		    do
			if ! $fix; then
			    echo WARNING: failed applying "$fix"
			    FINE=false
			    break
			fi
		    done
	    fi
	    if $FINE; then
		ls /usr/src/openldap/config-updates/$TARGET/patch/*.ldif \
		    2>/dev/null | while read ldif
		    do
			if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
				$ldif | ldapmodify -x \
				-H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
				-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
				-w "$OPENLDAP_ROOT_PASSWORD"; then
			    echo WARNING: failed importing $ldif, aborting
			    FINE=false
			    break
			fi
		    done
		unset GOSA_ACL_ROLE GOSA_ACL_USER
	    fi
	fi
	if $FINE; then
	    echo $TARGET >"$OPENLDAP_CONF_DIR/VERSION"
	elif test "$CRASH_ON_FAILED_UPDATES"; then
	    echo CRITICAL: configuration update failed >&2
	    echo bailing out, as CRASH_ON_FAILED_UPDATES was set >&2
	    exit 1
	else
	    echo WARNING: configuration update failed >&2
	    echo stop applying patches, starting slapd >&2
	    echo NOTICE: define CRASH_ON_FAILED_UPDATES to force container shutdown on such failures
	    echo proper deploymentconfig should then rollback to previous image
	fi
    done
    stop_ldapi
fi
if $SHOULD_IMPORT_BACKUP; then
    if ! grep -vE '^(result|search): ' /provision/init.ldif \
	    | slapadd -F "$OPENLDAP_CONF_DIR/slapd.d" -n 2; then
	echo CRITICAL: failed provisioning directory from backup
	exit 1
    fi
fi

unset APPLY_VERSIONS
