if test -z "$OPENLDAP_CONF_DIR" -o -z "$sfx"; then
    . /usr/src/openldap/db-env-init
fi

if $OPENLDAP_FULL_INIT; then
    should_init=false
    if test "$RESET_PASSWORD" -o "$RESET_SSL" = true; then
	should_init=true
    elif test -s "$MARK_FILE"; then
	for app in $APPS_LIST
	do
	    eval doapp=\${DO_$app}
	    test -z "$doapp" && doapp=false
	    if ! grep ^HAS_$app=$doapp "$MARK_FILE" >/dev/null; then
		should_init=true
		break
	    fi
	done
    else
	should_init=true
    fi
    if $should_init; then
	echo Refreshing LDAP Applications Configuration
	if start_with_ldapi; then
	    if ! reset-serviceaccounts; then
		echo WARNING: Failed Reconfiguring Service Accounts
	    fi
	    if $DO_LEMON; then
		if test -z "$LEAVE_LLNG_ALONE"; then
		    if ! llng-apps; then
			echo WARNING: Failed Upgrading LemonLDAP Configuration
		    fi
		fi
	    fi
	    stop_ldapi
	    markConfigured
	    echo
	    echo INFO: now running with:
	    cat $MARK_FILE
	else
	    echo WARNING: Failed Starting LDAP Reconfiguring Applications
	fi
    fi
    unset should_init
fi
