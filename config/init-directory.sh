if test -z "$OPENLDAP_CONF_DIR" -o -z "$sfx"; then
    . /usr/src/openldap/db-env-init
fi

if $OPENLDAP_FULL_INIT; then
    MONITOR_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_MONITOR_PASSWORD")
    SYNCREPL_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_SYNCREPL_PASSWORD")
    DC_PREFIX=$(echo "$OPENLDAP_ROOT_DN_SUFFIX" | sed 's|^dc=\([^,]*\),dc=.*$|\1|')
    OPENLDAP_ORG_SHORT=`echo "$OPENLDAP_ORG_SHORT" | sed 's| ||g'`
    if $DO_LEMON; then
	if test -x /usr/local/bin/getrsa; then
	    if test -s /certs/oidc.crt -a -s /certs/oidc.key; then
		cat /certs/oidc.crt >/tmp/saml.crt
		cat /certs/oidc.key >/tmp/saml.key
	    else
		openssl genrsa -out /tmp/saml.key 4096
		openssl rsa -pubout -in /tmp/saml.key -out /tmp/saml.crt
	    fi
	    eval `/usr/local/bin/getrsa`
	    if test "$RSA_PRIVATE"; then
		OPENLDAP_LEMON_OIDC_ENC_PRIVATE_KEY="$RSA_PRIVATE"
	    fi
	    if test "$RSA_PUBLIC"; then
		OPENLDAP_LEMON_OIDC_ENC_PUBLIC_KEY="$RSA_PUBLIC"
	    fi
	    if test "$RSA_PRIVATE"; then
		OPENLDAP_LEMON_OIDC_SIG_PRIVATE_KEY="$RSA_PRIVATE"
	    fi
	    if test "$RSA_PUBLIC"; then
		OPENLDAP_LEMON_OIDC_SIG_PUBLIC_KEY="$RSA_PUBLIC"
	    fi
	    unset RSA_PUBLIC RSA_PRIVATE
	    rm -f /tmp/saml.crt /tmp/saml.key

	    if test -s /certs/saml-sign.crt -a -s /certs/saml-sign.key; then
		cat /certs/saml-sign.crt >/tmp/saml.crt
		cat /certs/saml-sign.key >/tmp/saml.key
	    elif test -s /certs/saml.crt -a -s /certs/saml.key; then
		cat /certs/saml.crt >/tmp/saml.crt
		cat /certs/saml.key >/tmp/saml.key
	    else
		openssl req -nodes -new -x509 -days 3650 \
		    -keyout /tmp/saml.key -out /tmp/saml.crt -subj /CN=lemon
	    fi
	    eval `/usr/local/bin/getrsa`
	    if test "$RSA_PRIVATE"; then
		OPENLDAP_LEMON_SAML_SIG_PRIVATE_KEY="$RSA_PRIVATE"
	    fi
	    if test "$RSA_PUBLIC"; then
		OPENLDAP_LEMON_SAML_SIG_PUBLIC_KEY="$RSA_PUBLIC"
	    fi
	    unset RSA_PUBLIC RSA_PRIVATE

	    if test -s /certs/saml-sign.crt -a -s /certs/saml-sign.key; then
		cat /certs/saml-enc.crt >/tmp/saml.crt
		cat /certs/saml-enc.key >/tmp/saml.key
	    elif ! test -s /certs/saml.crt -a -s /certs/saml.key; then
		rm -f /tmp/saml.crt /tmp/saml.key
		openssl req -nodes -new -x509 -days 3650 \
		    -keyout /tmp/saml.key -out /tmp/saml.crt -subj /CN=lemon
	    fi
	    eval `/usr/local/bin/getrsa`
	    if test "$RSA_PRIVATE"; then
		OPENLDAP_LEMON_SAML_ENC_PRIVATE_KEY="$RSA_PRIVATE"
	    fi
	    if test "$RSA_PUBLIC"; then
		OPENLDAP_LEMON_SAML_ENC_PUBLIC_KEY="$RSA_PUBLIC"
	    fi
	    rm -f /tmp/saml.crt /tmp/saml.key
	    unset RSA_PUBLIC RSA_PRIVATE
	fi
	OIDC_KEY_ID_SIG="${OIDC_KEY_ID_SIG:-secretoidc}"
	OIDC_SIG_PUB=$(/bin/echo -en "{oidcServicePublicKeySig}$OPENLDAP_LEMON_OIDC_SIG_PUBLIC_KEY" | base64 -w 0)
	OIDC_SIG_PRIV=$(/bin/echo -en "{oidcServicePrivateKeySig}$OPENLDAP_LEMON_OIDC_SIG_PRIVATE_KEY" | base64 -w 0)
	SAML_ENC_PUB=$(/bin/echo -en "{samlServicePublicKeyEnc}$OPENLDAP_LEMON_SAML_ENC_PUBLIC_KEY" | base64 -w 0)
	SAML_ENC_PRIV=$(/bin/echo -en "{samlServicePrivateKeyEnc}$OPENLDAP_LEMON_SAML_ENC_PRIVATE_KEY" | base64 -w 0)
	SAML_SIG_PUB=$(/bin/echo -en "{samlServicePublicKeySig}$OPENLDAP_LEMON_SAML_SIG_PUBLIC_KEY" | base64 -w 0)
	SAML_SIG_PRIV=$(/bin/echo -en "{samlServicePrivateKeySig}$OPENLDAP_LEMON_SAML_SIG_PRIVATE_KEY" | base64 -w 0)
    fi
    if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
	    -e "s FIRST_PART $DC_PREFIX g" \
	    -e "s MONITOR_SA_PASSWORD_HASH $MONITOR_SA_PASSWORD_HASH g" \
	    -e "s SYNCREPL_SA_PASSWORD_HASH $SYNCREPL_SA_PASSWORD_HASH g" \
	    /usr/src/openldap/base.ldif \
	    | ldapadd -x -H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
	    -D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
	    -w "$OPENLDAP_ROOT_PASSWORD"; then
	echo CRITICAL: Failed initializing Directory
	exit 1
    fi
    if $DO_FUSION; then
	FUSION_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_FUSION_PASSWORD")
	if $DO_LEMON; then
	    FUSION_HEADER_AUTH=TRUE
	else
	    FUSION_HEADER_AUTH=FALSE
	fi
	if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
		-e "s FUSION_SA_PASSWORD_HASH $FUSION_SA_PASSWORD_HASH g" \
		-e "s HEADER_AUTH $FUSION_HEADER_AUTH g" \
		/usr/src/openldap/init-fusion.ldif \
		| ldapadd -x -H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
		-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
		-w "$OPENLDAP_ROOT_PASSWORD"; then
	    echo CRITICAL: Failed initializing FusionDirectory
	    exit 1
	fi
	date >"$OPENLDAP_CONF_DIR/.fusion-init"
    fi
    if $DO_WSWEET; then
	if test -z "$WSWEET_SA_PASSWORD_HASH"; then
	    WSWEET_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_WSWEET_PASSWORD")
	fi
	if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
		-e "s OPENLDAP_DOMAIN $OPENLDAP_ROOT_DOMAIN g" \
		-e "s WSWEET_SA_PASSWORD_HASH $WSWEET_SA_PASSWORD_HASH g" \
		/usr/src/openldap/init-wsweet.ldif \
		| ldapadd -x -H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
		-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
		-w "$OPENLDAP_ROOT_PASSWORD"; then
	    echo CRITICAL: Failed initializing Wsweet
	    exit 1
	fi
	date >"$OPENLDAP_CONF_DIR/.wsweet-init"
    fi
    if $DO_LEMON; then
	LEMONLDAP_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_LEMONLDAP_PASSWORD")
	LEMONLDAP_SESSIONS_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD")
	OPENLDAP_SSO_CLIENT_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_SSO_CLIENT_PASSWORD")
	if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
		-e "s|CSP_ALLOW|$CSP_ALLOW|g" \
		-e "s FIRST_PART $DC_PREFIX g" \
		-e "s HTTP_PORT $OPENLDAP_LEMON_HTTP_PORT g" \
		-e "s LEMONLDAP_HTTPS $OPENLDAP_LEMONLDAP_HTTPS g" \
		-e "s LEMONLDAP_PORT $LEMONLDAP_PORT g" \
		-e "s LEMONLDAP_PROTO $OPENLDAP_LEMONLDAP_PROTO g" \
		-e "s~LEMONLDAP_PASSWORD~$OPENLDAP_LEMONLDAP_PASSWORD~g" \
		-e "s LEMONLDAP_SA_PASSWORD_HASH $LEMONLDAP_SA_PASSWORD_HASH g" \
		-e "s~LEMONSESSIONS_PASSWORD~$OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD~g" \
		-e "s MAIL_DOMAIN $MAIL_DOMAIN g" \
		-e "s OPENLDAP_DOMAIN $OPENLDAP_ROOT_DOMAIN g" \
		-e "s OPENLDAP_HOST $OPENLDAP_HOST_ENDPOINT g" \
		-e "s OPENLDAP_PORT $OPENLDAP_BIND_LDAP_PORT g" \
		-e "s OPENLDAP_PROTO ldap g" \
		-e "s OPENLDAP_SMTP $OPENLDAP_SMTP_SERVER g" \
		-e "s|ORG_SHORT|$OPENLDAP_ORG_SHORT|g" \
		-e "s|OIDC_ENC_PUB_KEY|$OIDC_ENC_PUB|g" \
		-e "s|OIDC_ENC_PRIV_KEY|$OIDC_ENC_PRIV|g" \
		-e "s|OIDC_SIG_PUB_KEY|$OIDC_SIG_PUB|g" \
		-e "s|OIDC_SIG_PRIV_KEY|$OIDC_SIG_PRIV|g" \
		-e "s|OIDC_KEY_SIG|$OIDC_KEY_ID_SIG|g" \
		-e "s|SAML_ENC_PUB_KEY|$SAML_ENC_PUB|g" \
		-e "s|SAML_ENC_PRIV_KEY|$SAML_ENC_PRIV|g" \
		-e "s|SAML_SIG_PUB_KEY|$SAML_SIG_PUB|g" \
		-e "s|SAML_SIG_PRIV_KEY|$SAML_SIG_PRIV|g" \
		-e "s SESSIONS_SA_PASSWORD_HASH $LEMONLDAP_SESSIONS_SA_PASSWORD_HASH g" \
		-e "s BACKGROUND $LLNG_BACKGROUND g" \
		-e "s SKIN $LLNG_SKIN g" \
		-e "s#SUBJ_APPROVE#$PORTAL_APPROVE_SUBJECT#g" \
		-e "s#SUBJ_CONFIRM#$PORTAL_CONFIRM_SUBJECT#g" \
		-e "s#SUBJ_DONE#$PORTAL_DONE_SUBJECT#g" \
		-e "s#SUBJ_RSTCONFIRM#$PORTAL_RSTCONFIRM_SUBJECT#g" \
		-e "s#SUBJ_RSTDONE#$PORTAL_RSTDONE_SUBJECT#g" \
		-e "s SSO_CLIENT_SA_PASSWORD_HASH $OPENLDAP_SSO_CLIENT_SA_PASSWORD_HASH g" \
		-e "s LLNG_REGISTER_APPROVAL $ADMIN_CONTACT g" \
		/usr/src/openldap/init-lemon.ldif \
		| ldapadd -x -H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
		-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
		-w "$OPENLDAP_ROOT_PASSWORD"; then
	    echo CRITICAL: Failed initializing LemonLDAP-NG
	    exit 1
	fi
	date >"$OPENLDAP_CONF_DIR/.lemon-init"
    fi
    GOSA_ACL_ROLE=`/bin/echo -n "cn=fusion-admin,ou=aclroles,$OPENLDAP_ROOT_DN_SUFFIX" | base64 -w0`
    GOSA_ACL_USER=
    if test "$OPENLDAP_DEMO_PASSWORD"; then
	DEMO_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_DEMO_PASSWORD")
	if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
		-e "s|LDAP_DOMAIN|$OPENLDAP_ROOT_DOMAIN|g" \
		-e "s OPENLDAP_DEMO_PASSWORD_HASH $DEMO_SA_PASSWORD_HASH g" \
		/usr/src/openldap/demo.ldif \
		| ldapadd -x -H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
		-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
		-w "$OPENLDAP_ROOT_PASSWORD"; then
	    echo CRITICAL: Failed provisioning DEMO users
	    exit 1
	elif $DO_FUSION; then
	    GOSA_ACL_USER=`/bin/echo -n "uid=demoone,ou=users,$OPENLDAP_ROOT_DN_SUFFIX" | base64 -w0`
	fi
    elif test "$OPENLDAP_GLOBAL_ADMIN_PASSWORD"; then
	GLOBAL_ADMIN_SA_PASSWORD_HASH=$(slappasswd -s "$OPENLDAP_GLOBAL_ADMIN_PASSWORD")
	if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
		-e "s|LDAP_DOMAIN|$OPENLDAP_ROOT_DOMAIN|g" \
		-e "s OPENLDAP_GLOBAL_ADMIN_PASSWORD_HASH $GLOBAL_ADMIN_SA_PASSWORD_HASH g" \
		/usr/src/openldap/prod.ldif \
		| ldapadd -x -H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
		-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
		-w "$OPENLDAP_ROOT_PASSWORD"; then
	    echo CRITICAL: Failed provisioning initial Admin user
	    exit 1
	elif $DO_FUSION; then
	    GOSA_ACL_USER=`/bin/echo -n "uid=admin0,ou=users,$OPENLDAP_ROOT_DN_SUFFIX" | base64 -w0`
	fi
    else
	echo NOTICE: Not setting up users
    fi
    if test "$GOSA_ACL_USER"; then
	if ! sed -e "s OPENLDAP_SUFFIX $OPENLDAP_ROOT_DN_SUFFIX g" \
		-e "s GOSA_ACL_USER $GOSA_ACL_USER g" \
		-e "s GOSA_ACL_ROLE $GOSA_ACL_ROLE g" \
		/usr/src/openldap/configure_fusion.ldif \
		| ldapmodify -x \
		-H ldap://127.0.0.1:$OPENLDAP_BIND_LDAP_PORT/ \
		-D "$OPENLDAP_ROOT_DN_PREFIX,$OPENLDAP_ROOT_DN_SUFFIX" \
		-w "$OPENLDAP_ROOT_PASSWORD"; then
	    echo WARNING: Failed setting FusionDirectory initial ACL Entry
	fi
    fi
    unset BLUEMIND_SA_PASSWORD_HASH NEXTCLOUD_SA_PASSWORD_HASH \
	LEMONLDAP_SA_PASSWORD_HASH LEMONLDAP_SESSIONS_SA_PASSWORD_HASH \
	OPENLDAP_SSO_CLIENT_SA_PASSWORD_HASH DEMO_SA_PASSWORD_HASH \
	ROCKET_SA_PASSWORD_HASH SYNCREPL_SA_PASSWORD_HASH GOSA_ACL_ROLE \
	SAML_ENC_PUB SAML_ENC_PRIV SAML_SIG_PUB SAML_SIG_PRIV \
	OIDC_ENC_PUB OIDC_ENC_PRIV OIDC_SIG_PUB OIDC_SIG_PRIV \
	LEMONLDAP_PORT OPENLDAP_LEMONLDAP_PROTO GLOBAL_ADMIN_SA_PASSWORD_HASH \
	MONITOR_SA_PASSWORD_HASH GOSA_ACL_USER FUSION_SA_PASSWORD_HASH
fi
