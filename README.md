# k8s LDAP

OpenLDAP image, customized for StatefulSet auto-configuration, providing
with schema update capabilities, embedding LemonLDAP-NG, FusionDirectory,
SSH Public Keys, cloud & mail quotas, ...

Based on github.com/Worteks/docker-ldap.

Build with:

```
$ make build TARGET=rhel7 FLAVOR=ltb
$ make build TARGET=el7 FLAVOR=ltb
$ make build TARGET=deb10 FLAVOR=ltb
$ make build TARGET=el7 FLAVOR=distro
$ make build
```

Set `TARGET` switching Base OS (defaults to Debian10), or `FLAVOR` to pick
OpenLDAP packages source (either `ltb` for openldap-ltb, or `distro` to
rely on OS packages - defaults to openldap-ltb).

Run with:

```
$ make demo
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo TARGET=deb10 FLAVOR=ltb
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Schema extensions
-----------------

|  attribute OID                   | Name                                          | Description               | Equality           | Substring                    | Syntax                               | Single-Value | Introduced In | Last Patched | See                           |
| :------------------------------- | --------------------------------------------- | ------------------------- | ------------------ | ---------------------------- | ------------------------------------ | ------------ | ------------- | ------------ | ----------------------------- |
| `0.9.2342.19200300.100.1.3`      | `mail` `rfc822Mailbox`                        | RFC1274: RFC822 Mailbox   | caseIgnoreIA5Match | caseIgnoreIA5SubstringsMatch | `1.3.6.1.4.1.1466.115.121.1.26{256}` | Yes          | `0.0.0`       |              | config/ldif/single-mail.ldif  |
| `1.3.6.1.4.1.10098.1.1.12.6`     | `gosaMailAlternateAddress`                    | Email Aliases             | caseIgnoreIA5Match | caseIgnoreIA5SubstringsMatch | `1.3.6.1.4.1.1466.115.121.1.26`      | No           | `0.0.0`       |              | config/ldif/mail-fd.ldif      |
| `1.3.6.1.4.1.10098.1.1.12.7`     | `gosaMailForwardingAddress`                   | Emails to forward mail to | caseIgnoreIA5Match | caseIgnoreIA5SubstringsMatch | `1.3.6.1.4.1.1466.115.121.1.26`      | No           | `0.0.0`       |              | config/ldif/mail-fd.ldif      |
| `1.3.6.1.4.1.24552.500.1.1.1.13` | `sshPublicKey`                                | OpenSSH Public key        | octetStringMatch   |                              | `1.3.6.1.4.1.1466.115.121.1.40`      | No           | `0.0.0`       |              | config/ldif/openssh-lpk.ldif  |
| `1.3.6.1.4.1.53366.68.1.1`       | `mailQuota` `mailQuotaSize`                   | Mail Storage User Quota   | integerMatch       |                              | `1.3.6.1.4.1.1466.115.121.1.27`      | Yes          | `0.0.0`       |              | config/ldif/wsweet.ldif       |
| `1.3.6.1.4.1.53366.68.1.2`       | `cloudQuota` `ownCloudQuota` `nextCloudQuota` | Cloud Storage User Quota  | integerMatch       |                              | `1.3.6.1.4.1.1466.115.121.1.27`      | Yes          | `0.0.0`       |              | config/ldif/wsweet.ldif       |
| `1.3.6.1.4.1.53366.68.1.3`       | `usedMailQuota`                               | Used Cloud Storage        | integerMatch       |                              | `1.3.6.1.4.1.1466.115.121.1.27`      | Yes          | `0.0.0`       |              | config/ldif/wsweet.ldif       |
| `1.3.6.1.4.1.53366.68.1.4`       | `usedCloudQuota`                              | Used Mail Storage         | integerMatch       |                              | `1.3.6.1.4.1.1466.115.121.1.27`      | Yes          | `0.0.0`       |              | config/ldif/wsweet.ldif       |
| `1.3.6.1.4.1.53366.68.1.5`       | `profileId`                                   | Wsweet Profile Id         | caseIgnoreMatch    | caseIgnoreSubstringsMatch    | `1.3.6.1.4.1.1466.115.121.1.15`      | Yes          | `0.0.0`       |              | config/ldif/wsweet.ldif       |
| `1.3.6.1.4.1.53366.68.1.6`       | `keepIncomingMessages`                        | Keep incoming messages    | booleanMatch       |                              | `1.3.6.1.4.1.1466.115.121.1.7`       | Yes          | `0.0.0`       |              | config/ldif/wsweet.ldif       |
| `1.3.6.1.4.1.53366.68.1.7`       | `mailBackupAddress`                           | User Backup Email Address | caseIgnoreMatch    | caseIgnoreSubstringsMatch    | `1.3.6.1.4.1.1466.115.121.1.15`      | Yes          | `0.0.0`       |              | config/ldif/wsweet.ldif       |
| `1.3.6.1.4.1.53366.68.1.8`       | `nextCloudGSSBackend`                         | NextCloud GSS Backend     | caseIgnoreMatch    |                              | `1.3.6.1.4.1.1466.115.121.1.15`      | Yes          | `0.0.0`       |              | config/ldif/wsweet.ldif       |

|  objectclass OID               | Name                       | Must                                | May                                                                                                                | Auxiliary | Introduced In | Last Patched | See                             |
| :----------------------------- | -------------------------- | ----------------------------------- | ------------------------------------------------------------------------------------------------------------------ | --------- | ------------- | ------------ | ------------------------------- |
| `1.3.6.1.4.1.10098.1.2.1.19.5` | `gosaMailAccount`          | `mail`                              | `gosaMailAlternateAddress` `gosaMailForwardingAddress` `gosaMailMaxSize` `gosaMailQuota` `gosaMailServer` `...`    | Yes       | `0.0.0`       |              | config/ldif/mail-fd.ldif        |
| `1.3.6.1.4.1.10098.1.2.1.19.6` | `gosaAccount`              | `cn`                                |                                                                                                                    | Yes       | `0.0.0`       |              | config/ldif/configure_gosa.ldif |
| `1.3.6.1.4.1.10098.1.2.1.19.12`| `gosaGroupOfNames`         | `cn` `gosaGroupObjects`             | `member` `description`                                                                                             | Yes       | `0.0.0`       |              | config/ldif/core-fd.ldif        |
| `1.3.6.1.4.1.38414.10.2.5`     | `fdGroupMail`              | `mail`                              | `gosaMailAlternateAddress` `gosaMailForwardingAddress` `gosaMailMaxSize` `fdGroupMailLocalOnly` `gosaMailServer`   | Yes       | `0.0.0`       |              | config/ldif/mail-fd.ldif        |
| `1.3.6.1.4.1.24552.500.1.1.2.0`| `ldapPublicKey`            |                                     | `sshPublicKey` `uid`                                                                                               | Yes       | `0.0.0`       |              | config/ldif/openssh-lpk.ldif    |
| `1.3.6.1.4.1.39430.1.2.1`      | `wsweetUser`               |                                     | `mailQuota` `cloudQuota` `wsweetUsedMailQuota` `wsweetUsedCloudQuota` `wsweetMailBackupAddress` `wsweetProfileId`  | Yes       | `0.0.0`       |              | config/ldif/wsweet.ldif         |
| `1.3.6.1.4.1.39430.1.2.2`      | `wsweetGroupMailingList`   | `mail` `wsweetKeepIncomingMessages` | `mailQuota` `wsweetUsedMailQuota`                                                                                  | Yes       | `0.0.13`      |              | config/ldif/wsweet.ldif         |
| `1.3.6.1.4.1.39430.1.2.3`      | `wsweetSharedMailbox`      | `cn` `mail`                         | `mailQuota` `usedMailQuota` `profileId`                                                                            | Yes       | `0.0.13`      |              | config/ldif/wsweet.ldif         |


Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name                                |    Description                                 | Default                                                          |
| :---------------------------------------------- | ---------------------------------------------- | ---------------------------------------------------------------- |
|  `AF_DOMAIN`                                    | Artifactory FQDN                               | `artifactory.${OPENLDAP_ROOT_DOMAIN}`                            |
|  `ADM_DOMAIN`                                   | Admin FQDN                                     | `admin.${OPENLDAP_ROOT_DOMAIN}`                                  |
|  `AM_DOMAIN`                                    | AlertManager FQDN                              | `alertmanager.${OPENLDAP_ROOT_DOMAIN}`                           |
|  `ARA_DOMAIN`                                   | ARA FQDN                                       | `ara.${OPENLDAP_ROOT_DOMAIN}`                                    |
|  `AS_DOMAIN`                                    | Airsonic FQDN                                  | `airsonic.${OPENLDAP_ROOT_DOMAIN}`                               |
|  `AX_DOMAIN`                                    | Ansible AWX FQDN                               | `awx.${OPENLDAP_ROOT_DOMAIN}`                                    |
|  `BBB_DOMAIN`                                   | BigBlueButton / Greenlight FQDN                | `visio.${OPENLDAP_ROOT_DOMAIN}`                                  |
|  `CD_DOMAIN`                                    | CodiMD FQDN                                    | `codimd.${OPENLDAP_ROOT_DOMAIN}`                                 |
|  `CR_DOMAIN`                                    | ElasticSearch Comrade FQDN                     | `comrade.${OPENLDAP_ROOT_DOMAIN}`                                |
|  `DK_DOMAIN`                                    | DokuWiki FQDN                                  | `wiki.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `DO_ADMIN`                                     | OpenLDAP+LemonLDAP Wsweet Admin Toggle         | `false`                                                          |
|  `DO_ARA`                                       | OpenLDAP+LemonLDAP Ansible Run Analysis Toggle | `false`                                                          |
|  `DO_ALERTMANAGER`                              | OpenLDAP+LemonLDAP AlertManager Toggle         | `false`                                                          |
|  `DO_ARTIFACTORY`                               | OpenLDAP+LemonLDAP Artifactory Toggle          | `false`                                                          |
|  `DO_BLUEMIND`                                  | OpenLDAP+LemonLDAP BlueMind Toggle             | `false`                                                          |
|  `DO_AWX`                                       | OpenLDAP+LemonLDAP Ansible AWX Toggle          | `false`                                                          |
|  `DO_CODIMD`                                    | OpenLDAP+LemonLDAP CodiMD Toggle               | `false`                                                          |
|  `DO_COMRADE`                                   | OpenLDAP+LemonLDAP ES Comrade Toggle           | `false`                                                          |
|  `DO_CYRUS`                                     | OpenLDAP Cyrus Toggle                          | `false`                                                          |
|  `DO_DIASPORA`                                  | OpenLDAP+LemonLDAP Diaspora Toggle             | `false`                                                          |
|  `DO_DOKUWIKI`                                  | OpenLDAP+LemonLDAP DokuWiki Toggle             | `false`                                                          |
|  `DO_DRAW`                                      | OpenLDAP+LemonLDAP Draw Toggle                 | `false`                                                          |
|  `DO_ERRBIT`                                    | OpenLDAP+LemonLDAP Errbit Toggle               | `false`                                                          |
|  `DO_ETHERPAD`                                  | OpenLDAP+LemonLDAP EtherPad Toggle             | `false`                                                          |
|  `DO_EXPORTERS`                                 | OpenLDAP Monitor Service Account Toggle        | `false`                                                          |
|  `DO_FUSION`                                    | OpenLDAP+LemonLDAP FusionDirectory Toggle      | `false`                                                          |
|  `DO_GERRIT`                                    | OpenLDAP+LemonLDAP Gerrit Toggle               | `false`                                                          |
|  `DO_GITEA`                                     | OpenLDAP+LemonLDAP Gitea Toggle                | `false`                                                          |
|  `DO_GOGS`                                      | OpenLDAP+LemonLDAP Gogs Toggle                 | `false`                                                          |
|  `DO_GRAFANA`                                   | OpenLDAP+LemonLDAP Grafana Toggle              | `false`                                                          |
|  `DO_GREENLIGHT`                                | OpenLDAP+LemonLDAP Greelight Toggle            | `false`                                                          |
|  `DO_JENKINS`                                   | OpenLDAP+LemonLDAP Jenkins Toggle              | `false`                                                          |
|  `DO_JITSI`                                     | OpenLDAP+LemonLDAP Jitsi Toggle                | `false`                                                          |
|  `DO_KIBANA`                                    | OpenLDAP+LemonLDAP Kibana Toggle               | `false`                                                          |
|  `DO_KIWIIRC`                                   | OpenLDAP+LemonLDAP Kiwi IRC Toggle             | `false`                                                          |
|  `DO_LEMON`                                     | OpenLDAP+LemonLDAP Main Toggle                 | `false`                                                          |
|  `DO_MAILDROP`                                  | OpenLDAP+LemonLDAP Maildrop Toggle             | `false`                                                          |
|  `DO_MAILHOG`                                   | OpenLDAP+LemonLDAP Mailhog Toggle              | `false`                                                          |
|  `DO_MASTODON`                                  | OpenLDAP+LemonLDAP Mastodon Toggle             | `false`                                                          |
|  `DO_MATOMO`                                    | OpenLDAP+LemonLDAP Matomo Toggle               | `false`                                                          |
|  `DO_MATTERMOST`                                | OpenLDAP+LemonLDAP Mattermost Toggle           | `false`                                                          |
|  `DO_MEDUSA`                                    | OpenLDAP+LemonLDAP Medusa Toggle               | `false`                                                          |
|  `DO_MINIO`                                     | OpenLDAP+LemonLDAP MinIO Toggle                | `false`                                                          |
|  `DO_MINIOCONSOLE`                              | OpenLDAP+LemonLDAP MinIO Console Toggle        | `false`                                                          |
|  `DO_MUNIN`                                     | OpenLDAP+LemonLDAP Munin Toggle                | `false`                                                          |
|  `DO_NEXTCLOUD`                                 | OpenLDAP+LemonLDAP NextCloud Toggle            | `false`                                                          |
|  `DO_NEXUS`                                     | OpenLDAP+LemonLDAP Nexus Toggle                | `false`                                                          |
|  `DO_NEWZNAB`                                   | OpenLDAP+LemonLDAP Newznab Toggle              | `false`                                                          |
|  `DO_PEERTUBE`                                  | OpenLDAP+LemonLDAP PeerTube Toggle             | `false`                                                          |
|  `DO_PHPLDAPADMIN`                              | OpenLDAP+LemonLDAP PHP LDAP Admin Toggle       | `false`                                                          |
|  `DO_PROMETHEUS`                                | OpenLDAP+LemonLDAP Prometheus Toggle           | `false`                                                          |
|  `DO_REGISTRYCONSOLE`                           | OpenLDAP+LemonLDAP Registry Console Toggle     | `false`                                                          |
|  `DO_RELEASEBELL`                               | OpenLDAP+LemonLDAP ReleaseBell Toggle          | `false`                                                          |
|  `DO_ROCKET`                                    | OpenLDAP+LemonLDAP RocketChat Toggle           | `false`                                                          |
|  `DO_ROUNDCUBE`                                 | OpenLDAP+LemonLDAP Roundcube Toggle            | `false`                                                          |
|  `DO_RSPAMD`                                    | LemonLDAP RSpamd Toggle                        | `false`                                                          |
|  `DO_SABNZBD`                                   | OpenLDAP+LemonLDAP Sabnzbd Toggle              | `false`                                                          |
|  `DO_SERVICEDESK`                               | OpenLDAP+LemonLDAP ServiceDesk Toggle          | `false`                                                          |
|  `DO_SOGO`                                      | OpenLDAP+LemonLDAP SOGo Toggle                 | `false`                                                          |
|  `DO_SONARQUBE`                                 | OpenLDAP+LemonLDAP SonarQube Toggle            | `false`                                                          |
|  `DO_SOPLANNING`                                | OpenLDAP+LemonLDAP SOPlanning Toggle           | `false`                                                          |
|  `DO_SSP`                                       | OpenLDAP+LemonLDAP SelfServicePassword Toggle  | `false`                                                          |
|  `DO_SYMPA`                                     | OpenLDAP+LemonLDAP Sympa Toggle                | `false`                                                          |
|  `DO_SYSPASS`                                   | OpenLDAP+LemonLDAP SysPass Toggle              | `false`                                                          |
|  `DO_THANOS`                                    | OpenLDAP+LemonLDAP Thanos Toggle               | `false`                                                          |
|  `DO_THANOS_BUCKET`                             | OpenLDAP+LemonLDAP Thanos Bucket Web Toggle    | `false`                                                          |
|  `DO_THANOS_RECEIVE`                            | OpenLDAP+LemonLDAP Thanos Receive Toggle       | `false`                                                          |
|  `DO_TINYTINYRSS`                               | OpenLDAP+LemonLDAP TinyTinyRSS Toggle          | `false`                                                          |
|  `DO_TRANSMISSION`                              | OpenLDAP+LemonLDAP Transmission Toggle         | `false`                                                          |
|  `DO_WEKAN`                                     | OpenLDAP+LemonLDAP Wekan Toggle                | `false`                                                          |
|  `DO_WHITEPAGES`                                | OpenLDAP+LemonLDAP WhitePages Toggle           | `false`                                                          |
|  `DO_WORDPRESS`                                 | OpenLDAP+LemonLDAP WordPress Toggle            | `false`                                                          |
|  `DO_WSWEET`                                    | OpenLDAP+LemonLDAP Wsweet Toggle               | `false`                                                          |
|  `DO_ZPUSH`                                     | OpenLDAP Z-push Toggle                         | `false`                                                          |
|  `DR_DOMAIN`                                    | Draw FQDN                                      | `draw.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `DS_DOMAIN`                                    | Diaspora FQDN                                  | `diaspora.${OPENLDAP_ROOT_DOMAIN}`                               |
|  `EB_DOMAIN`                                    | Errbit FQDN                                    | `errbit.${OPENLDAP_ROOT_DOMAIN}`                                 |
|  `EC_DOMAIN`                                    | EtherCalc FQDN                                 | `calc.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `EP_DOMAIN`                                    | EtherPad FQDN                                  | `pad.${OPENLDAP_ROOT_DOMAIN}`                                    |
|  `FD_DOMAIN`                                    | FusionDirectory FQDN                           | `fusiondirectory.${OPENLDAP_ROOT_DOMAIN}`                        |
|  `GF_DOMAIN`                                    | Grafana FQDN                                   | `grafana.${OPENLDAP_ROOT_DOMAIN}`                                |
|  `GR_DOMAIN`                                    | Gerrit FQDN                                    | `gerrit.${OPENLDAP_ROOT_DOMAIN}`                                 |
|  `GTA_DOMAIN`                                   | Gitea FQDN                                     | `gitea.${OPENLDAP_ROOT_DOMAIN}`                                  |
|  `GGS_DOMAIN`                                   | Gogs FQDN                                      | `gogs.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `JK_DOMAIN`                                    | Jenkins FQDN                                   | `jenkins.${OPENLDAP_ROOT_DOMAIN}`                                |
|  `JM_DOMAIN`                                    | Jitsi FQDN                                     | `meet.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `KB_DOMAIN`                                    | Kibana FQDN                                    | `kibana.${OPENLDAP_ROOT_DOMAIN}`                                 |
|  `KW_DOMAIN`                                    | Kiwi IRC FQDN                                  | `irc.${OPENLDAP_ROOT_DOMAIN}`                                    |
|  `LEAVE_LLNG_ALONE`                             | Disables LemonLDAP-NG Configuration upgrades   | undef                                                            |
|  `LLNG_APPNAME`                                 | LemonLDAP-NG Portal Name                       | `KubeLemon`                                                      |
|  `LLNG_BACKGROUND`                              | LemonLDAP-NG Portal Background Image           | `wall_dark.jpg`                                                  |
|  `LLNG_LANG`                                    | LemonLDAP-NG Menu Language                     | `en`                                                             |
|  `LLNG_POSTGRES_SESSIONS_DATABASE`              | LemonLDAP-NG Postgres Sessions Database        | `llngsessions`                                                   |
|  `LLNG_POSTGRES_SESSIONS_HOST`                  | LemonLDAP-NG Postgres Sessions Host            | `postgres`                                                       |
|  `LLNG_POSTGRES_SESSIONS_PASSWORD`              | LemonLDAP-NG Postgres Sessions Password        | `secret`                                                         |
|  `LLNG_POSTGRES_SESSIONS_PORT`                  | LemonLDAP-NG Postgres Sessions Port            | `5432`                                                           |
|  `LLNG_POSTGRES_SESSIONS_SSL`                   | LemonLDAP-NG Postgres Sessions SSL Mode        | `disable`                                                        |
|  `LLNG_POSTGRES_SESSIONS_TABLE`                 | LemonLDAP-NG Postgres Sessions Table           | `sessions`                                                       |
|  `LLNG_POSTGRES_SESSIONS_USER`                  | LemonLDAP-NG Postgres Sessions User            | `lemon`                                                          |
|  `LLNG_REDIS_SESSIONS_DATABASE`                 | LemonLDAP-NG Redis Sessions Database           | `1`                                                              |
|  `LLNG_REDIS_SESSIONS_HOST`                     | LemonLDAP-NG Redis Sessions Host Address       | `redis`                                                          |
|  `LLNG_REDIS_SESSIONS_PASSWORD`                 | LemonLDAP-NG Redis Sessions Password           | ``                                                               |
|  `LLNG_REDIS_SESSIONS_PORT`                     | LemonLDAP-NG Redis Sessions Port               | `6379`                                                           |
|  `LLNG_SENTINEL_SESSIONS_HOST`                  | LemonLDAP-NG Sentinel Sessions Host Address    | undef                                                            |
|  `LLNG_SENTINEL_SESSIONS_PORT`                  | LemonLDAP-NG Sentinel Sessions Port            | `26379`                                                          |
|  `LLNG_SENTINEL_SESSIONS_REPLICASET`            | LemonLDAP-NG Sentinel Sessions ReplicaSet Name | undef, defaults to `LLNG_SENTINEL_SESSIONS_HOST`                 |
|  `LLNG_SESSIONS_BACKEND`                        | LemonLDAP-NG Sessions Backend                  | `ldap`                                                           |
|  `LLNG_SKIN`                                    | LemonLDAP-NG Skin                              | `kubelemon`                                                      |
|  `MAIL_DOMAIN`                                  | BlueMind FQDN                                  | `mail.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `MC_DOMAIN`                                    | MinIO Console FQDN                             | `minio-console.${OPENLDAP_ROOT_DOMAIN}`                          |
|  `MD_DOMAIN`                                    | Maildrop FQDN                                  | `maildrop.${OPENLDAP_ROOT_DOMAIN}`                               |
|  `MDS_DOMAIN`                                   | Maildrop Haraka Stats FQDN                     | `maildrop-stats.${OPENLDAP_ROOT_DOMAIN}`                         |
|  `MH_DOMAIN`                                    | Mailhog FQDN                                   | `mailhog.${OPENLDAP_ROOT_DOMAIN}`                                |
|  `MI_DOMAIN`                                    | MinIO FQDN                                     | `s3.${OPENLDAP_ROOT_DOMAIN}`                                     |
|  `MM_DOMAIN`                                    | Mattermost FQDN                                | `mattermost.${OPENLDAP_ROOT_DOMAIN}`                             |
|  `MN_DOMAIN`                                    | Munin FQDN                                     | `munin.${OPENLDAP_ROOT_DOMAIN}`                                  |
|  `MO_DOMAIN`                                    | Mastodon FQDN                                  | `mastodon.${OPENLDAP_ROOT_DOMAIN}`                               |
|  `MS_DOMAIN`                                    | Medusa FQDN                                    | `medusa.${OPENLDAP_ROOT_DOMAIN}`                                 |
|  `MT_DOMAIN`                                    | Matomo FQDN                                    | `matomo.${OPENLDAP_ROOT_DOMAIN}`                                 |
|  `NC_DOMAIN`                                    | NextCloud FQDN                                 | `cloud.${OPENLDAP_ROOT_DOMAIN}`                                  |
|  `NN_DOMAIN`                                    | Newznab FQDN                                   | `newznab.${OPENLDAP_ROOT_DOMAIN}`                                |
|  `NX_DOMAIN`                                    | Nexus FQDN                                     | `nexus.${OPENLDAP_ROOT_DOMAIN}`                                  |
|  `OPENLDAP_ALERTMANAGER_OAUTH2_SECRET`          | OpenLDAP AlertManager Oauth2 Secret            | `secret`                                                         |
|  `OPENLDAP_ARA_OAUTH2_SECRET`                   | OpenLDAP Ara Oauth2 Secret                     | `secret`                                                         |
|  `OPENLDAP_ARA_PASSWORD`                        | OpenLDAP Ara Password                          | `secret`                                                         |
|  `OPENLDAP_ARTIFACTORY_PASSWORD`                | OpenLDAP Artifactory Password                  | `secret`                                                         |
|  `OPENLDAP_AUTHPROXY_PASSWORD`                  | OpenLDAP AuthProxy Password                    | `secret`                                                         |
|  `OPENLDAP_AWX_PASSWORD`                        | OpenLDAP AWX Password                          | `secret`                                                         |
|  `OPENLDAP_BIND_LDAP_PORT`                      | OpenLDAP Plaintext Bind Port                   | `1389`                                                           |
|  `OPENLDAP_BIND_LDAPS_PORT`                     | OpenLDAP TLS Bind Port                         | `1636`                                                           |
|  `OPENLDAP_BLUEMIND_PASSWORD`                   | OpenLDAP BlueMind Password                     | `secret`                                                         |
|  `OPENLDAP_CODIMD_PASSWORD`                     | OpenLDAP CodiMD Password                       | `secret`                                                         |
|  `OPENLDAP_CYRUS_PASSWORD`                      | OpenLDAP Cyrus Password                        | `secret`                                                         |
|  `OPENLDAP_DB_BACKEND`                          | OpenLDAP Main Database Backend                 | `mdb`                                                            |
|  `OPENLDAP_DEBUG_LEVEL`                         | OpenLDAP Server Debug Level                    | `256`                                                            |
|  `OPENLDAP_DEMO_PASSWORD`                       | Password for OpenLDAP Demo Accounts            | undef, defining it toggles Demo Accounts creation                |
|  `OPENLDAP_DOKUWIKI_PASSWORD`                   | OpenLDAP DokuWiki Password                     | `secret`                                                         |
|  `OPENLDAP_DRAW_OAUTH2_SECRET`                  | OpenLDAP Draw Oauth2 Secret                    | `secret`                                                         |
|  `OPENLDAP_ERRBIT_OAUTH2_SECRET`                | OpenLDAP Errbit Oauth2 Secret                  | `secret`                                                         |
|  `OPENLDAP_ERRBIT_PASSWORD`                     | OpenLDAP Errbit Password                       | `secret`                                                         |
|  `OPENLDAP_ESCOMRADE_OAUTH2_SECRET`             | OpenLDAP Comrade Oauth2 Secret                 | `secret`                                                         |
|  `OPENLDAP_FUSION_OAUTH2_SECRET`                | OpenLDAP FusionDirectory Oauth2 Secret         | `secret`                                                         |
|  `OPENLDAP_FUSION_PASSWORD`                     | OpenLDAP FusionDirectory Password              | `secret`                                                         |
|  `OPENLDAP_GITEA_OAUTH2_SECRET`                 | OpenLDAP Gitea Oauth2 Secret                   | `secret`                                                         |
|  `OPENLDAP_GERRIT_PASSWORD`                     | OpenLDAP Gerrit Password                       | `secret`                                                         |
|  `OPENLDAP_GRAFANA_OAUTH2_SECRET`               | OpenLDAP Grafana Oauth2 Secret                 | `secret`                                                         |
|  `OPENLDAP_GRAFANA_PASSWORD`                    | OpenLDAP Grafana Password                      | `secret`                                                         |
|  `OPENLDAP_GREENLIGHT_OAUTH2_SECRET`            | OpenLDAP Greenlight Oauth2 Secret              | `secret`                                                         |
|  `OPENLDAP_GREENLIGHT_PASSWORD`                 | OpenLDAP Greenlight Password                   | `secret`                                                         |
|  `OPENLDAP_GLOBAL_ADMIN_PASSWORD`               | Password for OpenLDAP Global Admin             | undef, defining it toggles Global Admin Account creation         |
|  `OPENLDAP_HOST_ENDPOINT`                       | OpenLDAP Endpoint configuring LemonLDAP        | `openldap`                                                       |
|  `OPENLDAP_HOSTNAME`                            | OpenLDAP Hostname - testing replication        | Container/Pod hostname                                           |
|  `OPENLDAP_INIT_DEBUG_LEVEL`                    | OpenLDAP Server Bootstrap Debug Level          | `256`                                                            |
|  `OPENLDAP_INITIAL_ID`                          | Initial Public Endpoint / federation           | undef                                                            |
|  `OPENLDAP_JENKINS_PASSWORD`                    | OpenLDAP Jenkins Password                      | `secret`                                                         |
|  `OPENLDAP_JITSI_PASSWORD`                      | OpenLDAP Jitsi Password                        | `secret`                                                         |
|  `OPENLDAP_KIBANA_OAUTH2_SECRET`                | OpenLDAP Kibana Oauth2 Secret                  | `secret`                                                         |
|  `OPENLDAP_KIWIIRC_OAUTH2_SECRET`               | OpenLDAP Kiwi IRC Oauth2 Secret                | `secret`                                                         |
|  `OPENLDAP_LEMONLDAP_HTTPS`                     | LemonLDAP HTTPS Toggle                         | undef, defining it toggles HTTPS-related configuration           |
|  `OPENLDAP_LEMONLDAP_PASSWORD`                  | OpenLDAP LemonLDAP Password                    | `secret`                                                         |
|  `OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD`         | OpenLDAP LemonLDAP Sessions Storage            | `secret`                                                         |
|  `OPENLDAP_LEMON_HTTP_PORT`                     | OpenLDAP LemonLDAP Reload HTTP Port            | `8080`                                                           |
|  `OPENLDAP_MATOMO_OAUTH2_SECRET`                | OpenLDAP Matomo Oauth2 Secret                  | `secret`                                                         |
|  `OPENLDAP_MATOMO_PASSWORD`                     | OpenLDAP Matomo Password                       | `secret`                                                         |
|  `OPENLDAP_MATTERMOST_OAUTH2_SECRET`            | OpenLDAP Mattermost Oauth2 Secret              | `secret`                                                         |
|  `OPENLDAP_MATTERMOST_PASSWORD`                 | OpenLDAP Mattermost Password                   | `secret`                                                         |
|  `OPENLDAP_MEDUSA_OAUTH2_SECRET`                | OpenLDAP Medusa Oauth2 Secret                  | `secret`                                                         |
|  `OPENLDAP_MEDUSA_PASSWORD`                     | OpenLDAP Medusa Password                       | `secret`                                                         |
|  `OPENLDAP_MONITOR_PASSWORD`                    | OpenLDAP Monitor Password                      | `secret`                                                         |
|  `OPENLDAP_MUNIN_OAUTH2_SECRET`                 | OpenLDAP Munin Oauth2 Secret                   | `secret`                                                         |
|  `OPENLDAP_MUNIN_PASSWORD`                      | OpenLDAP Munin Password                        | `secret`                                                         |
|  `OPENLDAP_MY_PUBLIC_ID`                        | Cluster Public Endpoint / federation           | undef                                                            |
|  `OPENLDAP_NEXTCLOUD_PASSWORD`                  | OpenLDAP NextCloud Password                    | `secret`                                                         |
|  `OPENLDAP_NEXUS_PASSWORD`                      | OpenLDAP Nexus Password                        | `secret`                                                         |
|  `OPENLDAP_ORG_SHORT`                           | LemonLDAP Organization Name                    | Based on `OPENLDAP_ROOT_DOMAIN`, default produces `demo`         |
|  `OPENLDAP_PEERTUBE_OAUTH2_SECRET`              | OpenLDAP PeerTube Oauth2 Secret                | `secret`                                                         |
|  `OPENLDAP_PEERTUBE_PASSWORD`                   | OpenLDAP PeerTube Password                     | `secret`                                                         |
|  `OPENLDAP_PHPLDAPADMIN_PASSWORD`               | OpenLDAP PHP LDAP Admin Password               | `secret`                                                         |
|  `OPENLDAP_PROMETHEUS_OAUTH2_SECRET`            | OpenLDAP Prometheus Oauth2 Secret              | `secret`                                                         |
|  `OPENLDAP_REGISTRYCONSOLE_OAUTH2_SECRET`       | OpenLDAP Registry Console Oauth2 Secret        | `secret`                                                         |
|  `OPENLDAP_REGISTRYCONSOLE_PASSWORD`            | OpenLDAP Registry Consolel Password            | `secret`                                                         |
|  `OPENLDAP_RELEASEBELL_PASSWORD`                | OpenLDAP ReleaseBell Password                  | `secret`                                                         |
|  `OPENLDAP_ROCKET_PASSWORD`                     | OpenLDAP Rocket Password                       | `secret`                                                         |
|  `OPENLDAP_ROOT_DN_RREFIX`                      | OpenLDAP `olcRootDN` Prefix                    | `cn=admin`                                                       |
|  `OPENLDAP_ROOT_DN_SUFFIX`                      | OpenLDAP `olcSuffix` Suffix                    | seds `OPENLDAP_ROOT_DOMAIN`, default produces `dc=demo,dc=local` |
|  `OPENLDAP_ROOT_DOMAIN`                         | Directory & LemonLDAP-NG Apps Root Domain Name | `demo.local`                                                     |
|  `OPENLDAP_ROOT_PASSWORD`                       | OpenLDAP `olcRootPW` Password                  | `secret`                                                         |
|  `OPENLDAP_SABNZBD_OAUTH2_SECRET`               | OpenLDAP SABnzbd Oauth2 Secret                 | `secret`                                                         |
|  `OPENLDAP_SABNZBD_PASSWORD`                    | OpenLDAP SABnzbd Password                      | `secret`                                                         |
|  `OPENLDAP_SERVICEDESK_OAUTH2_SECRET`           | OpenLDAP ServiceDesk Oauth2 Secret             | `secret`                                                         |
|  `OPENLDAP_SERVICEDESK_PASSWORD`                | OpenLDAP ServiceDesk Password                  | `secret`                                                         |
|  `OPENLDAP_SMTP_PORT`                           | LemonLDAP SMTP port                            | `25`                                                             |
|  `OPENLDAP_SMTP_SERVER`                         | LemonLDAP SMTP relay                           | `smtp.demo.local`                                                |
|  `OPENLDAP_SOGO_PASSWORD`                       | OpenLDAP SOGo Password                         | `secret`                                                         |
|  `OPENLDAP_SONARQUBE_PASSWORD`                  | OpenLDAP SonarQube Password                    | `secret`                                                         |
|  `OPENLDAP_SOPLANNING_PASSWORD`                 | OpenLDAP SOPlanning Password                   | `secret`                                                         |
|  `OPENLDAP_SSO_CLIENT_PASSWORD`                 | OpenLDAP Generic SSO/SAML Password             | `secret`                                                         |
|  `OPENLDAP_SSP_OAUTH2_SECRET`                   | OpenLDAP SelfServicePassword Oauth2 Secret     | `secret`                                                         |
|  `OPENLDAP_SSP_PASSWORD`                        | OpenLDAP SelfServicePassword Password          | `secret`                                                         |
|  `OPENLDAP_STATEFULSET_NAME`                    | OpenLDAP StatefulSet Name - setting up repl    | `openldap`                                                       |
|  `OPENLDAP_STATIC_NEIGHBORS`                    | List of OpenLDAP Remote Addresses / federation | undef                                                            |
|  `OPENLDAP_SYNCREPL_PASSWORD`                   | OpenLDAP Syncrepl Password                     | `secret`                                                         |
|  `OPENLDAP_SYMPA_PASSWORD`                      | OpenLDAP Sympa Password                        | `secret`                                                         |
|  `OPENLDAP_SYSPASS_PASSWORD`                    | OpenLDAP SysPass Password                      | `secret`                                                         |
|  `OPENLDAP_THANOS_OAUTH2_SECRET`                | OpenLDAP Thanos Oauth2 Secret                  | `secret`                                                         |
|  `OPENLDAP_THANOSBUCKET_OAUTH2_SECRET`          | OpenLDAP Thanos Bucket Oauth2 Secret           | `secret`                                                         |
|  `OPENLDAP_THANOSRECEIVE_OAUTH2_SECRET`         | OpenLDAP Thanos Receive Oauth2 Secret          | `secret`                                                         |
|  `OPENLDAP_TINYTINYRSS_OAUTH2_SECRET`           | OpenLDAP TinyTinyRSS Oauth2 Secret             | `secret`                                                         |
|  `OPENLDAP_TINYTINYRSS_PASSWORD`                | OpenLDAP TinyTinyRSS Password                  | `secret`                                                         |
|  `OPENLDAP_TRANSMISSION_OAUTH2_SECRET`          | OpenLDAP Transmission Oauth2 Secret            | `secret`                                                         |
|  `OPENLDAP_WEKAN_OAUTH2_SECRET`                 | OpenLDAP Wekan Oauth2 Secret                   | `secret`                                                         |
|  `OPENLDAP_WEKAN_PASSWORD`                      | OpenLDAP Wekan Password                        | `secret`                                                         |
|  `OPENLDAP_WHITEPAGES_OAUTH2_SECRET`            | OpenLDAP WhitePages Oauth2 Secret              | `secret`                                                         |
|  `OPENLDAP_WHITEPAGES_PASSWORD`                 | OpenLDAP WhitePages Password                   | `secret`                                                         |
|  `OPENLDAP_WORDPRESS_PASSWORD`                  | OpenLDAP WordPress Password                    | `secret`                                                         |
|  `OPENLDAP_WSWEET_PASSWORD`                     | OpenLDAP Wsweet Password                       | `secret`                                                         |
|  `OPENLDAP_ZPUSH_PASSWORD`                      | OpenLDAP Z-Push Password                       | `secret`                                                         |
|  `RB_DOMAIN`                                    | ReleaseBell FQDN                               | `releasebell.${OPENLDAP_ROOT_DOMAIN}`                            |
|  `RK_DOMAIN`                                    | Roundcube FQDN                                 | `roundcube.${OPENLDAP_ROOT_DOMAIN}`                              |
|  `RESET_PASSWORDS`                              | Resets OpenLDAP Optional Accounts Passwords    | undef, forces resets Service Accounts passwords                  |
|  `RESET_SSL`                                    | Resets OpenLDAP CA, Certificate & Key          | undef, forces resets OpenLDAP TLS Configuration                  |
|  `PM_DOMAIN`                                    | Prometheus FQDN                                | `prometheus.${OPENLDAP_ROOT_DOMAIN}`                             |
|  `PT_DOMAIN`                                    | PeerTube FQDN                                  | `peertube.${OPENLDAP_ROOT_DOMAIN}`                               |
|  `RC_DOMAIN`                                    | RocketChat FQDN                                | `chat.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `RG_DOMAIN`                                    | Docker Registry Console FQDN                   | `registry-console.${OPENLDAP_ROOT_DOMAIN}`                       |
|  `RK_DOMAIN`                                    | Roundcube FQDN                                 | `roundcube.${OPENLDAP_ROOT_DOMAIN}`                              |
|  `RS_DOMAIN`                                    | RSpamd FQDN                                    | `rspamd.${OPENLDAP_ROOT_DOMAIN}`                                 |
|  `SB_DOMAIN`                                    | SABnzbd FQDN                                   | `sabnzbd.${OPENLDAP_ROOT_DOMAIN}`                                |
|  `SD_DOMAIN`                                    | LTB ServiceDesk FQDN                           | `servicedesk.${OPENLDAP_ROOT_DOMAIN}`                            |
|  `SG_DOMAIN`                                    | SOGo FQDN                                      | `sogo.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `SM_DOMAIN`                                    | Sympa FQDN                                     | `lists.${OPENLDAP_ROOT_DOMAIN}`                                  |
|  `SO_DOMAIN`                                    | SOPlanning FQDN                                | `planning.${OPENLDAP_ROOT_DOMAIN}`                               |
|  `SP_DOMAIN`                                    | LTB SelfServicePassword FQDN                   | `password.${OPENLDAP_ROOT_DOMAIN}`                               |
|  `SQ_DOMAIN`                                    | SonarQube FQDN                                 | `sonarqube.${OPENLDAP_ROOT_DOMAIN}`                              |
|  `SY_DOMAIN`                                    | SysPass FQDN                                   | `syspass.${OPENLDAP_ROOT_DOMAIN}`                                |
|  `SZ_DOMAIN`                                    | SABnzbd Completed Downloads FQDN               | `sab-completed.${OPENLDAP_ROOT_DOMAIN}`                          |
|  `TN_DOMAIN`                                    | Thanos FQDN                                    | `thanos.${OPENLDAP_ROOT_DOMAIN}`                                 |
|  `TNB_DOMAIN`                                   | Thanos Bucket Web FQDN                         | `thanos-bucket.${OPENLDAP_ROOT_DOMAIN}`                          |
|  `TNR_DOMAIN`                                   | Thanos Receive FQDN                            | `thanos-receive.${OPENLDAP_ROOT_DOMAIN}`                         |
|  `TR_DOMAIN`                                    | TinyTinyRSS FQDN                               | `tinytinyrss.${OPENLDAP_ROOT_DOMAIN}`                            |
|  `TX_DOMAIN`                                    | Transmission FQDN                              | `transmission.${OPENLDAP_ROOT_DOMAIN}`                           |
|  `TZ_DOMAIN`                                    | Transmission Completed Downloads FQDN          | `transmission-completed.${OPENLDAP_ROOT_DOMAIN}`                 |
|  `UNMANAGED`                                    | Toggles provisioning scripts                   | undef, any non-empty value would disable init scripts            |
|  `WH_DOMAIN`                                    | LTB WhitePages FQDN                            | `whitepages.${OPENLDAP_ROOT_DOMAIN}`                             |
|  `WK_DOMAIN`                                    | Wekan FQDN                                     | `wekan.${OPENLDAP_ROOT_DOMAIN}`                                  |
|  `WP_DOMAIN`                                    | WordPress FQDN                                 | `blog.${OPENLDAP_ROOT_DOMAIN}`                                   |
|  `ZP_DOMAIN`                                    | Z-Push FQDN                                    | `zpush.${OPENLDAP_ROOT_DOMAIN}`                                  |


The following table details the possible debug levels.

| Debug Level | Description                                   |
| ----------- | --------------------------------------------- |
| -1          | Enable all debugging                          |
|  0          | Enable no debugging                           |
|  1          | Trace function calls                          |
|  2          | Debug packet handling                         |
|  4          | Heavy trace debugging                         |
|  8          | Connection management                         |
|  16         | Log packets sent and recieved                 |
|  32         | Search filter processing                      |
|  64         | Configuration file processing                 |
|  128        | Access control list processing                |
|  256        | Stats log connections, operations and results |
|  512        | Stats log entries sent                        |
|  1024       | Log communication with shell backends         |
|  2048       | Log entry parsing debugging                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point                 | Description                                         |
| :---------------------------------- | --------------------------------------------------- |
|  `/certs`                           | OpenLDAP Certificate (optional)                     |
|  `/etc/openldap`                    | OpenLDAP configuration directory (distro ldap @el)  |
|  `/etc/ldap`                        | OpenLDAP configuration directory (distro ldap @deb) |
|  `/provision`                       | Database Import Directory (looks for init.ldif)     |
|  `/usr/local/openldap/etc/openldap` | OpenLDAP configuration directory (ltb-openldap)     |
|  `/var/lib/ldap`                    | OpenLDAP data directory                             |

Cluster Auto Configuration
---------------------------

This image can be used setting up N-Way clusters.

It has been made with Kubernetes StatefulSets in mind, and would assume that
members from a cluster are named according to a deployment name and an
incremental numeric identifier. Assuming our statefulset is named `openldap` and
has `3` replicas, in the `sample` project, then the following DNS records would
eventually identify our cluster members:

 * openldap-0.openldap.sample.svc
 * openldap-1.openldap.sample.svc
 * openldap-2.openldap.sample.svc

Knowing this, containers started from our image would check for their hostname.
If we can match such a numeric identifier suffixing our hostname, then we would
try and detect other members, starting from `0` and incrementing a counter
until `ldapsearch` fails querying for an OpenLDAP service.

Having identified potentials members to join our cluster, our containers would
check for a `/etc/openldap/.repl-configured` file, which holds the list of
members we've already configured replication with. For every detected neighbor
missing in that list, we would add an `olcSyncRepl` entry to the hdb database.

This process has a critical implication: bootstrapping a cluster, using a Serial
deployment policy would be recommended, ensuring services start in an orderly
manner. The first node would boot and provision either the demo or production
initial dataset. Then the second one would setup replication against the first
one. And the third one, against the first and second nodes.

At which point, we would want to reboot the first member of our cluster, such
as it would detect the second and third nodes, setting up its replication.
And eventually, the second node, ensuring it would have a link replicating
data from the third node.

There is no easy way to know about a StatefulSet size from a Kubernetes
container point of view. DNS records for non-existing members of a cluster would
still resolve, there is no environment variable, the only way to know for sure
would be to use the OpenShift client querying its API. Although doing so would
imply adding +150-200M binary to our image, which doesn't make much sense.

Federated Cluster
------------------

To deploy an OpenLDAP cluster on top of federated OpenShift/Kubernetes clusters,
assuming the ability of deploying LoadBalancer services (which depends on some
cloud integration to a cloud provider offering with TCP loadbalancers, or an
Ingress IPs pool when cloud integration is disabled), we would use the
following:

```
cluster1$ oc process -f deploy/openshift/fed-endpoints.yaml | oc apply -f-
cluster1$ oc get svc openldap-demo

clusterN$ oc process -f deploy/openshift/fed-endpoints.yaml | oc apply -f-
clusterN$ oc get svc openldap-demo
```

Note the IP or FQDNs allocated to those LoadBalancer Services, as we would use
them setting up our deployment:

```
cluster1$ oc process -f deploy/openshift/run-fed-init.yaml \
    -p OPENLDAP_INITIAL_ID=<cluster1-public-lb-endpoint> \
    -p OPENLDAP_STATIC_NEIGHBORS="<list-of-all-lb-endpoints>"| oc apply -f-
cluster1$ oc get pods -w
```

Once our initial LDAP member is done initializing, we would then deploy our
remaining cluster members:

```
clusterN$ oc process -f deploy/openshift/run-fed-main.yaml \
    -p OPENLDAP_INITIAL_ID=<first-public-lb-endpoint> \
    -p OPENLDAP_MY_PUBLIC_ID=<clusterN-public-lb-endpoit> \
    -p OPENLDAP_STATIC_NEIGHBORS="<list-of-all-lb-endpoints>"| oc apply -f-
```

Updating Schemas
-----------------

Updating existing databases can be done applying LDIFs while booting a new image.

In the `./config/config-updates` folder, we may include our patches:

```
$ ls config/config-updates
0.0.1  0.0.2  0.0.3  ...  0.0.X
```

All OpenLDAP servers would have a file `/etc/openldap/VERSION` (or, in Debian
based images, `/etc/ldap/VERSION`), marking which patch was last applied,
allowing us to update schemas refreshing our images. Create your own version:

```
$ mkdir -p config/config-updates/0.0.42
```

Depending on what we'll want to update, we could create several folders in
there. Say we want to apply changes to the `cn=config` database, then we
would create a `cn=config` sub-directory. Those changes would be applied
*to all OpenLDAP members* of a cluster, as a first step applying a new version.

```
$ mkdir -p config/config-updates/0.0.42/cn=config
$ cat <<EOF >config/config-updates/0.0.42/cn=config/00-acl.ldif
dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to attrs=userPassword xxxx
EOF
```

The second step applying an update would be to load new schemas. Doing so, we
would create a `schemas` sub-directory. Schemas are loaded
*on all OpenLDAP members* of a cluster. Loading OpenLDAP main schemas, we could
link them out of `/etc/openldap/schemas`, instead of re-installing new copies:

```
$ mkdir -p config/config-updates/0.0.42/schemas
$ ln -sf /etc/openldap/schemas/nis.ldif config/config-updates/0.0.42/schemas/00-nis.ldif
```

Once our OpenLDAP configuration and schemas are up-to-date, we may want to apply
patches to our hdb databases. Those updates would only apply
*on the first OpenLDAP member* of a cluster, as they are meant to be replicated
and, as such, shouldn't be applied twice. We could store ldifs creating new
objects into a `main` sub-directory:

```
$ mkdir -p config/config-updates/0.0.42/main
$ cat <<EOF >config/config-updates/0.0.42/main/00-dokuwiki.ldif
dn: cn=dokuwiki,ou=services,OPENLDAP_SUFFIX
objectClass: top
objectClass: person
cn: dokuwiki
description: Service Account for DokuWiki
sn: DokuWiki service account
userPassword: DOKUWIKI_SA_PASSWORD_HASH
EOF
```

That being done, we could want to run a few shell scripts applying some logic
plain ldifs won't be able to offer:

```
$ mkdir -p config/config-updates/0.0.42/scripts
$ cat <<EOF >config/config-updates/0.0.42/scripts/00-do-something.sh
#!/bin/sh

ldapsearch -D cn=wsweet,ou=services,\$OPENLDAP_ROOT_DN .... | while read line
    do
	if test something; then
	    ldapmodify xxx
	fi
    done

exit \$?
EOF
$ chmod +x config/config-updates/0.0.42/scripts/00-do-something.sh
```

Finally, we may want to apply ldifs patching existing objects. A `patch`
sub-directory could be used:

```
$ mkdir -p config/config-updates/0.0.42/patch
$ cat <<EOF >config/config-updates/0.0.42/patch/00-ppolicy.ldf
dn: cn=autoLockout,ou=policies,OPENLDAP_SUFFIX
changetype: modify
replace: pwdSafeModify
pwdSafeModify: FALSE
EOF
```

Having installed all our ldifs, you'ld have noticed we used several placeholders
such as `OPENLDAP_SUFFIX`, or `DOKUWIKI_SA_PASSWORD_HASH`. We would want to
check for their proper substitions with runtime variables:

```
$ vi ./config/run-openldap
```
