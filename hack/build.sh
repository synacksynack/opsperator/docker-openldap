#!/bin/sh -e

OS=$1
FLAVOR=$2
DOCKERFILE_PATH=
BASE_IMAGE_NAME=opsperator/

trap "rm -f $DOCKERFILE_PATH.version" INT QUIT EXIT

docker_build_with_version()
{
    local dockerfile="$1" imagename="$2"
    if test -z "$imagename"; then
	imagename=$IMAGE_NAME
    fi
    DOCKERFILE_PATH=$(perl -MCwd -e 'print Cwd::abs_path shift' $dockerfile)
    cp $DOCKERFILE_PATH "$DOCKERFILE_PATH.version"
    git_version=$(git rev-parse --short HEAD)
    echo "LABEL io.openshift.builder-version=\"$git_version\"" >>"$DOCKERFILE_PATH.version"
    docker build -t $imagename -f "$DOCKERFILE_PATH.version" .
    if test "$SKIP_SQUASH" != "1"; then
	squash "$DOCKERFILE_PATH.version"
    fi
    rm -f "$DOCKERFILE_PATH.version"
}

squash()
{
    easy_install -q --user docker_py==1.6.0 docker-scripts==0.4.4
    base=$(awk '/^FROM/{print $2}' $1)
    $HOME/.local/bin/docker-scripts squash -f $base $IMAGE_NAME
}

IMAGE_NAME=${BASE_IMAGE_NAME}openldap
if test "$TEST_MODE"; then
    IMAGE_NAME="$IMAGE_NAME-candidate"
fi
echo "-> Building $IMAGE_NAME ..."
if test "$FLAVOR" = ltb; then
    if test "$OS" = rhel7 -o "$OS" = rhel7-candidate; then
	docker_build_with_version Dockerfile.ltb-rhel7
    elif test "$OS" = centos7 -o "$OS" = centos7-candidate; then
	docker_build_with_version Dockerfile.ltb-el7
    elif test "$OS" = debian10 -o "$OS" = debian10-candidate; then
	docker_build_with_version Dockerfile.ltb-deb10
    else
	docker_build_with_version Dockerfile.ltb
    fi
elif test "$FLAVOR" = ltbsrc; then
    echo "---> Building Base ${BASE_IMAGE_NAME}ldap-bdb ..."
    docker_build_with_version Dockerfile.ltbsrc-deb10-bdb ${BASE_IMAGE_NAME}ldap-bdb
    echo "---> Building Intermediary ${BASE_IMAGE_NAME}ldap-build ..."
    docker_build_with_version Dockerfile.ltbsrc-deb10-ldap ${BASE_IMAGE_NAME}ldap-build
    echo "---> Building Final $IMAGE_NAME ..."
    docker_build_with_version Dockerfile.ltbsrc-deb10-base
elif test "$OS" = rhel7 -o "$OS" = rhel7-candidate; then
    docker_build_with_version Dockerfile.distro-rhel7
elif test "$OS" = centos7 -o "$OS" = centos7-candidate; then
    docker_build_with_version Dockerfile.distro-el7
elif test "$OS" = debian10 -o "$OS" = debian10-candidate; then
    docker_build_with_version Dockerfile.distro-deb10
else
    docker_build_with_version Dockerfile
fi
